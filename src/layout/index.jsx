/* eslint-disable react/prop-types */
import React from 'react';
import AppBar from 'components/AppBar';
import Drawer from 'components/Drawer';
import styled from 'styled-components';
import Hidden from '@material-ui/core/Hidden';
import { useTheme } from '@material-ui/core/styles';

const Content = styled.main`
  flex-grow: 1;
  padding: 24px;
  overflow-y: auto;
`;

const drawerWidth = 260;

const Nav = styled.nav`
  ${({ $theme }) => `${[$theme.breakpoints.up('sm')]} {
      width: ${drawerWidth}px;
      flex-shrink: 0;
    }`};
`;

const Toolbar = styled.div`
  min-height: 56px;

  @media (min-width: 0px) and (orientation: landscape) {
    min-height: 48px;
  }

  @media (min-width: 600px) {
    min-height: 64px;
  }
`;

const Layout = ({
  layoutTheme,
  handleLight,
  handleDark,
  children,
  user,
  ...rest
}) => {
  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const theme = useTheme();

  const container =
    window !== undefined ? () => window.document.body : undefined;

  return (
    <>
      <AppBar
        position="fixed"
        handleDrawerToggle={handleDrawerToggle}
        drawerWidth={drawerWidth}
        layoutTheme={layoutTheme}
        handleLight={handleLight}
        handleDark={handleDark}
        user={user}
      />
      <Nav $theme={theme} aria-label="mailbox folders">
        <Hidden smUp implementation="css">
          <Drawer
            drawerWidth={drawerWidth}
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true
            }}
          />
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer drawerWidth={drawerWidth} variant="permanent" open />
        </Hidden>
      </Nav>
      <Content>
        <Toolbar />
        {children}
      </Content>
    </>
  );
};

export default Layout;
