const lightTheme = {
  name: 'light',
  colors: {
    base: '#fcfcfc',
    paper: '#fff',
    table: '#fff',
    tableAlternate: '#f5f5f5',
    tableBorder: '#e0e0e0',
    baseFont: '#3C4852',
    baseFontAlternate: '#3C4852',
    border: '#f0f4f7',
    chat: '#ddf9c6'
  }
};
const darkTheme = {
  name: 'dark',
  colors: {
    base: '#131417',
    paper: '#202022',
    table: '#424242',
    tableAlternate: '#515151',
    tableBorder: '#515151',
    baseFont: '#EBEBF5',
    baseFontAlternate: '#3C4852',
    border: '#2c2c2e',
    chat: '#131c22'
  }
};
export { lightTheme, darkTheme };
