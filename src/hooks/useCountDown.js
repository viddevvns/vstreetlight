import { useEffect } from 'react';
import { calculateTimeLeft } from 'utils/getDateString';

const useCountDown = (setTimeLeft, date) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft(date));
    }, 1000);

    return () => clearTimeout(timer);
  });
};

export default useCountDown;
