import { faultTypes } from '../constants';

const initialState = { images: {} };

const reducerMap = {
  [`${faultTypes.FETCH_FAULTS_LIST}_REQUESTED`](state, { towerId }) {
    return {
      ...state,
      [towerId]: []
    };
  },
  [`${faultTypes.FETCH_FAULTS_LIST}_RECEIVED`](state, { payload, towerId }) {
    const { resp } = payload;
    return {
      ...state,
      [towerId]: resp[towerId].Faults
    };
  },
  [`${faultTypes.FETCH_FAULT_DETAILS}_RECEIVED`](state, { payload, faultId }) {
    const { resp } = payload;
    return {
      ...state,
      [faultId]: resp[faultId]
    };
  },
  [`${faultTypes.FETCH_FAULT_IMAGES}_RECEIVED`](state, { payload, faultId }) {
    const { resp } = payload;
    return {
      ...state,
      images: {
        ...state.images,
        [faultId]: resp[faultId].images
      }
    };
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
