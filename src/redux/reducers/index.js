import { combineReducers } from 'redux';
import { loadingBarReducer } from 'react-redux-loading-bar';
import towers from './towers';
import lines from './lines';
import faults from './faults';
import maintenance from './maintenance';
import uploader from './uploader';
import analytics from './analytics';

export default combineReducers({
  loadingBar: loadingBarReducer,
  towers,
  lines,
  faults,
  maintenance,
  uploader,
  analytics
});
