import { lineTypes } from '../constants';

const initialState = {};

const reducerMap = {
  [`${lineTypes.FETCH_LINES_LIST}_REQUESTED`](state, { lineId }) {
    return {
      ...state,
      [lineId]: []
    };
  },
  [`${lineTypes.FETCH_LINES_LIST}_RECEIVED`](state, { payload, lineId }) {
    const { resp } = payload;
    return {
      ...state,
      [lineId]: resp
    };
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
