import { towerTypes } from '../constants';

const initialState = {};

const reducerMap = {
  [`${towerTypes.FETCH_TOWER_INFO}_REQUESTED`](state, { towerId }) {
    return {
      ...state,
      [towerId]: {}
    };
  },
  [`${towerTypes.FETCH_TOWER_INFO}_RECEIVED`](state, { payload, towerId }) {
    const { resp } = payload;
    return {
      ...state,
      [towerId]: resp
    };
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
