import { maintenanceTypes } from '../constants';

const initialState = { users: [], tasks: {}, labels: {}, chats: {} };

const reducerMap = {
  [`${maintenanceTypes.FETCH_MAINTENANCE_INFO}_REQUESTED`](state, { faultId }) {
    return {
      ...state,
      [faultId]: {}
    };
  },
  [`${maintenanceTypes.FETCH_MAINTENANCE_INFO}_RECEIVED`](
    state,
    { payload, faultId }
  ) {
    const { resp } = payload;
    return {
      ...state,
      [faultId]: resp[faultId]
    };
  },
  [`${maintenanceTypes.FETCH_MAINTENANCE_USERS}_RECEIVED`](state, { payload }) {
    const { resp } = payload;
    return {
      ...state,
      users: Object.values(resp)
    };
  },
  [`${maintenanceTypes.FETCH_MAINTENANCE_TASK_PROGRESS}_RECEIVED`](
    state,
    { payload, faultId }
  ) {
    const { resp } = payload;
    return {
      ...state,
      [faultId]: {
        ...state[faultId],
        status: resp[faultId]
      }
    };
  },
  [`${maintenanceTypes.FETCH_MAINTENANCE_TASK}_RECEIVED`](
    state,
    { payload, username }
  ) {
    const { resp } = payload;
    const tasks = resp.map((task) => ({
      ...task,
      'Tower ID': task['Fault ID'].slice(0, 5)
    }));
    return {
      ...state,
      [username]: tasks
    };
  },
  [`${maintenanceTypes.FETCH_TASK_DETAILS}_RECEIVED`](
    state,
    { payload, faultId }
  ) {
    const { resp } = payload;
    return {
      ...state,
      tasks: { ...state.tasks, [faultId]: resp }
    };
  },
  [`${maintenanceTypes.FETCH_STEPPER_LABELS}_RECEIVED`](
    state,
    { payload, faultId }
  ) {
    const { resp } = payload;
    return {
      ...state,
      labels: { ...state.labels, [faultId]: resp[faultId] }
    };
  },
  [`${maintenanceTypes.GET_TASK_CHAT}_RECEIVED`](state, { payload, faultId }) {
    const { resp } = payload;

    return {
      ...state,
      chats: { ...state.chats, [faultId]: resp[faultId] }
    };
  },
  [`${maintenanceTypes.POST_TASK_CHAT}_RECEIVED`](state, { payload, faultId }) {
    const { resp } = payload;
    debugger;
    return {
      ...state,
      chats: { ...state.chats, [faultId]: resp[faultId] }
    };
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
