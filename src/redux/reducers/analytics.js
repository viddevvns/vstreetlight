import { analyticsTypes } from '../constants';

const initialState = {};

const reducerMap = {
  [`${analyticsTypes.FETCH_ANALYTIC_DETAILS}_RECEIVED`](state, { payload }) {
    const { resp } = payload;
    return resp;
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
