import { uploaderTypes } from '../constants';

const initialState = {};

const reducerMap = {
  [`${uploaderTypes.FETCH_PRESIGNED_URLS}_RECEIVED`](
    state,
    { payload, faultId }
  ) {
    const { resp } = payload;
    return {
      ...state,
      [faultId]: resp[faultId]?.presigned_urls
    };
  }
};

const reducer = (state = initialState, action) => {
  const actionHandler = reducerMap[action.type];
  return actionHandler ? actionHandler(state, action) : state;
};

export default reducer;
