import { uploaderTypes } from '../constants';

export const getPresignedUrls = (faultId, counter) => ({
  type: uploaderTypes.FETCH_PRESIGNED_URLS,
  endpoint: `vMaintenance/maintenance_task_image_upload/${faultId}/${counter}`,
  method: 'get',
  faultId
});
