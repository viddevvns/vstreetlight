import { maintenanceTypes } from '../constants';

export const getMaintenanceInfo = (faultId) => ({
  type: maintenanceTypes.FETCH_MAINTENANCE_INFO,
  endpoint: `vMaintenance/maintenance_details/${faultId}`,
  method: 'get',
  faultId
});

export const getMaintenanceUsers = () => ({
  type: maintenanceTypes.FETCH_MAINTENANCE_USERS,
  endpoint: 'vMaintenance/personnels_details/',
  method: 'get'
});

export const assignMaintenanceTask = (params) => ({
  type: maintenanceTypes.ASSIGN_MAINTENANCE_TASK,
  endpoint: 'vMaintenance/generate_tickets/',
  method: 'post',
  params
});

export const getMaintenanceTaskProgress = (faultId) => ({
  type: maintenanceTypes.FETCH_MAINTENANCE_TASK_PROGRESS,
  endpoint: `vMaintenance/maintenance_task_progress_status/${faultId}`,
  method: 'get',
  faultId
});

export const getMaintenanceTask = (username) => ({
  type: maintenanceTypes.FETCH_MAINTENANCE_TASK,
  endpoint: `vMaintenance/maintenance_task_personnel/${username}`,
  method: 'get',
  username
});

export const getTaskHistory = (faultId) => ({
  type: maintenanceTypes.FETCH_TASK_DETAILS,
  endpoint: `vMaintenance/maintenance_task_operations_details/${faultId}`,
  method: 'get',
  faultId
});

export const updateTaskOperations = (params) => ({
  type: maintenanceTypes.UPDATE_TASK_OPERATIONS,
  endpoint: 'vMaintenance/maintenance_task_operations_details',
  method: 'post',
  params
});

export const getStepperLabels = (faultId) => ({
  type: maintenanceTypes.FETCH_STEPPER_LABELS,
  endpoint: `vMaintenance/maintenance_task_progress_detials/${faultId}`,
  method: 'get',
  faultId
});

export const updatePreparedness = (params) => ({
  type: maintenanceTypes.UPDATE_PREPAREDNESS,
  endpoint: 'vMaintenance/maintenance_task_progress',
  method: 'post',
  params
});

export const updateTaskStatus = (params) => ({
  type: maintenanceTypes.UPDATE_TASK_STATUS,
  endpoint: 'vMaintenance/maintenance_task_status',
  method: 'post',
  params
});

export const postTaskChat = (params, faultId) => ({
  type: maintenanceTypes.POST_TASK_CHAT,
  endpoint: 'vMaintenance/maintenance_task_chat',
  method: 'post',
  params,
  faultId
});

export const getTaskChat = (faultId) => ({
  type: maintenanceTypes.GET_TASK_CHAT,
  endpoint: `vMaintenance/maintenance_task_chat/${faultId}`,
  method: 'get',
  faultId
});
