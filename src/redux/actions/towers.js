import { towerTypes } from '../constants';

export const getTowerInfo = (towerId) => ({
  type: towerTypes.FETCH_TOWER_INFO,
  endpoint: `vPB_Street/sl_details/${towerId}`,
  method: 'get',
  towerId
});
