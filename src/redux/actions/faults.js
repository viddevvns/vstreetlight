import { faultTypes } from '../constants';

export const getFaultList = (towerId) => ({
  type: faultTypes.FETCH_FAULTS_LIST,
  endpoint: `vPB_Street/sl_activity/${towerId}`,
  method: 'get',
  towerId
});

export const getFaultImages = (faultId) => ({
  type: faultTypes.FETCH_FAULT_IMAGES,
  endpoint: `vHealthComponent/fault_images/${faultId}`,
  method: 'get',
  faultId
});

export const getFaultDetails = (faultId) => ({
  type: faultTypes.FETCH_FAULT_DETAILS,
  endpoint: `vHealthComponent/fault_details/${faultId}`,
  method: 'get',
  faultId
});
