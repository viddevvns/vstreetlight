import { lineTypes } from '../constants';

export const getLineList = (lineId) => ({
  type: lineTypes.FETCH_LINES_LIST,
  endpoint: 'vPB_Street/sl_list_full/*',
  method: 'get',
  lineId
});
