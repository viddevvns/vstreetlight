import { analyticsTypes } from '../constants';

export const getAnalyticDetails = () => ({
  type: analyticsTypes.FETCH_ANALYTIC_DETAILS,
  endpoint: 'vAnalytics/*',
  method: 'get'
});
