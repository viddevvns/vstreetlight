import { useDispatch, useSelector } from 'react-redux';

const getLines = (lineId) => useSelector(({ lines }) => lines[lineId]);
