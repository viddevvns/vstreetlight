import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { loadingBarMiddleware } from 'react-redux-loading-bar';

import { httpMiddleware } from './middlewares';
import rootReducer from './reducers';

const withMiddleWare = applyMiddleware(
  httpMiddleware,
  loadingBarMiddleware({
    promiseTypeSuffixes: ['REQUESTED', 'RECEIVED', 'FAILED']
  })
);

// const withDevTools = process.env.isDev
//   ? composeWithDevTools(withMiddleWare)
//   : withMiddleWare;

const withDevTools = composeWithDevTools(withMiddleWare);

export default createStore(rootReducer, withDevTools);
