import axios from 'axios';

const host = 'https://vnautilus.vidrona.com/';

const httpMiddleware = (store) => (next) => (action) => {
  if (!action || !action.endpoint || !action.method) {
    return next(action);
  }
  const { endpoint, method, type, params, query, ...rest } = action;

  let url = `${host}${endpoint}`;
  let queryString = '';

  if (query) queryString = encodeURIComponent(JSON.stringify(params));
  if (queryString) url = `${url}?${queryString}`;

  const headers = {
    'Content-Type': 'application/json'
  };
  const actionPromise = fetch(url, {
    method,
    body: JSON.stringify(params),
    headers
  });

  next({
    type: type + '_REQUESTED',
    ...rest
  });

  return actionPromise
    .then((response) => response.json())
    .then((payload) =>
      next({
        type: type + '_RECEIVED',
        payload,
        ...rest
      })
    )
    .catch((error) =>
      next({
        type: type + '_FAILED',
        payload: error,
        ...rest
      })
    );
};

export default httpMiddleware;
