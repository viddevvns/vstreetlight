/* eslint-disable react/prop-types */
import React from 'react';
import { Routes as ReactRoutes, Route } from 'react-router-dom';
import ClientInfo from 'containers/ClientInfo';
import Home from 'containers/Home';
import MaintenanceAnalysis from 'containers/MaintenanceAnalysis';
import AssetsPerformance from 'containers/AssetsPerformance';
import LineAvailability from 'containers/LineAvailability';
import AssetDetails from 'containers/AssetDetails';
// import AssetInfo from 'containers/AssetInfo';
import CurrentProject from 'containers/CurrentProject';
import Project from 'containers/Project';
import ProtectedRoute from 'components/ProtectedRoute';
import Unauthorized from 'components/Unauthorized';
import VegetativeHealth from 'containers/VegetativeHealth';
import LineDetails from 'containers/LineDetails';
import TowerDetails from 'containers/TowerDetails';
import FaultDetails from 'containers/FaultDetails';
import MaintenanceWork from 'containers/MaintenanceWork';
import Survey from 'containers/Survey';
import GNSS from 'containers/GNSS';
import Conductor from 'containers/Conductor';
import AssignedTasks from 'containers/AssignedTasks';
import OPGW from 'containers/OPGW';
import StreetLightMaps from 'containers/StreetLightMaps';
import StreetLightsSatellite from 'containers/StreetLightsSatellite';
import Reliability from 'containers/Reliability';
import SatelliteView from 'containers/SatelliteView';
import CCMSDetails from 'containers/CCMS/CCMSDetails';
import CCMSList from 'containers/CCMS/CCMSList';

const routes = [
  // {
  //   path: '/',
  //   element: <Home breadCrumb={[{ label: 'Home', path: '/' }]} />
  // },
  {
    path: '/client-info',
    element: ClientInfo,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Client Info', path: '/client_info' }
    ]
  },
  {
    path: '/my_task',
    element: AssetDetails,
    url: '/my_task/:lineId/assigned_tasks',
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'My Task', path: '/my_task' },
      { label: 'Asset Details', path: '/my_task' }
    ]
  },
  {
    path: '/my_task/:lineId/assigned_tasks',
    element: AssignedTasks,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'My Task', path: '/my_task' },
      { label: 'Asset Details', path: '/my_task' },
      { label: 'lineId', path: '/my_task/:lineId/assigned_tasks' }
    ]
  },
  {
    path: '/my_task/:lineId/assigned_tasks/:faultId',
    element: FaultDetails,
    exact: true,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'My Task', path: '/my_task' },
      { label: 'Asset Details', path: '/my_task' },
      { label: 'lineId', path: '/my_task/:lineId/assigned_tasks' },
      { label: 'faultId', path: '/my_task/:lineId/assigned_tasks/:faultId' }
    ]
  },
  {
    path: '/ccms_details',
    element: CCMSDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'CCMS Details', path: '/ccms_details' },
    ]
  },
  {
    path: '/ccms_details/:lineId',
    element: CCMSList,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'CCMS Details', path: '/ccms_details' },
      { label: 'CCMS List', path: '/ccms_details/:lineId' },
    ]
  },
  {
    path: '/ccms_analytics',
    element: OPGW,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'CCMS Analytics', path: '/ccms_analytics' }
    ]
  },
  {
    path: '/maintenance_work',
    element: MaintenanceWork,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Maintenance Work', path: '/maintenance_work' }
    ]
  },
  {
    path: '/survey',
    element: Survey,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Survey', path: '/survey' }
    ]
  },
  {
    path: '/projects/current_project',
    element: CurrentProject,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Projects', path: '' },
      { label: 'Current Project', path: '/projects/current_project' }
    ]
  },
  {
    path: '/projects/current_project/:projectId',
    element: Project,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Projects', path: '' },
      { label: 'Current Project', path: '/projects/current_project' },
      { label: 'projectId', path: '/projects/current_project/:projectId' }
    ]
  },
  {
    path: '/projects/current_project/:projectId/GNSS',
    element: GNSS,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Projects', path: '' },
      { label: 'Current Project', path: '/projects/current_project' },
      { label: 'projectId', path: '/projects/current_project/:projectId' },
      { label: 'GNSS', path: '/projects/current_project/:projectId/GNSS' }
    ]
  },
  {
    path: '/analysis/maintenance_analysis',
    element: MaintenanceAnalysis,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Analysis', path: '' },
      {
        label: 'Maintenance Analysis',
        path: '/analysis/maintenance_analysis'
      }
    ]
  },
  {
    path: '/analysis/assets_performance',
    element: AssetsPerformance,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Analysis', path: '' },
      { label: 'Assets Performance', path: '/analysis/assets_performance' }
    ]
  },
  {
    path: '/analysis/line_availability',
    element: LineAvailability,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Analysis', path: '' },
      { label: 'Line Availability', path: '/analysis/line_availability' }
    ]
  },
  {
    path: '/asset_health/asset_details',
    element: AssetDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_details' },
      { label: 'Asset Details', path: '/asset_health/asset_details' }
    ]
  },
  {
    path: '/asset_health/asset_info',
    element: LineDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_info' },
      { label: 'Asset Info', path: '/asset_health/asset_info' }
    ]
  },
  {
    path: '/asset_health/ccms_details',
    element: CCMSDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/ccms_details' },
      { label: 'CCMS Details', path: '/asset_health/ccms_details' }
    ]
  },
  {
    path: '/asset_health/asset_details/:lineId',
    element: LineDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_details' },
      { label: 'Asset Details', path: '/asset_health/asset_details' },
      { label: 'lineId', path: '/asset_health/asset_details/:lineId' }
    ]
  },
  {
    path: '/asset_health/asset_details/:lineId/:towerId',
    element: TowerDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_details' },
      { label: 'Asset Details', path: '/asset_health/asset_details' },
      { label: 'lineId', path: '/asset_health/asset_details/:lineId' },
      { label: 'towerId', path: '/asset_health/asset_details/:lineId/:towerId' }
    ]
  },
  {
    path: '/asset_health/asset_details/:lineId/:towerId/:faultId',
    element: FaultDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_details' },
      { label: 'Asset Details', path: '/asset_health/asset_details' },
      { label: 'lineId', path: '/asset_health/asset_details/:lineId' },
      {
        label: 'towerId',
        path: '/asset_health/asset_details/:lineId/:towerId'
      },
      {
        label: 'faultId',
        path: '/asset_health/asset_details/:lineId/:towerId/:faultId'
      }
    ]
  },
  {
    path: '/asset_health/asset_details/:lineId/:towerId/:faultId/task',
    element: FaultDetails,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Asset Health', path: '/asset_health/asset_details' },
      { label: 'Asset Details', path: '/asset_health/asset_details' },
      { label: 'lineId', path: '/asset_health/asset_details/:lineId' },
      {
        label: 'towerId',
        path: '/asset_health/asset_details/:lineId/:towerId'
      },
      {
        label: 'faultId',
        path: '/asset_health/asset_details/:lineId/:towerId/:faultId'
      },
      {
        label: 'Task',
        path: '/asset_health/asset_details/:lineId/:towerId/:faultId/task'
      }
    ]
  },
  {
    path: '/vegetative_health',
    element: VegetativeHealth,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Vegetative Health', path: '/vegetative_health/' }
    ]
  },
  {
    path: '/streetlightmaps',
    element: StreetLightMaps,
    breadCrumb: [
      { label: 'Home', path: '/' },
      { label: 'Street Lights Maps', path: '/streetlightmaps/' }
    ]
  },
  {
    path: '/StreetLightsSatellite',
    element: StreetLightsSatellite,
    breadCrumb: [
      {
        label: 'Home',
        path: '/'
      },
      {
        label: 'Street Lights Satellite Monitoring',
        path: '/StreetLightsSatellite/'
      }
    ]
  },
  {
    path: '/reliability',
    element: Reliability,
    breadCrumb: [],
    exact: true
  },
  {
    path: '/satellite-view',
    element: SatelliteView,
    breadCrumb: [],
    exact: true
  }
];

const Routes = ({ handleLogout, handleLogin, user }) => {
  return (
    <ReactRoutes>
      <Route
        path="/"
        handleLogin={handleLogin}
        element={<Home user={user} handleLogin={handleLogin} />}
      />
      <Route path="/unauthorized" element={<Unauthorized />} />
      {routes.map(({ path, element, ...rest }) => (
        <ProtectedRoute
          key={path}
          path={path}
          handleLogout={handleLogout}
          Component={element}
          user={user}
          {...rest}
        />
        // <Route key={path} path={path} element={element} />
      ))}
    </ReactRoutes>
  );
};

export default Routes;
