/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUICard from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import MUICardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import LocationOnRoundedIcon from '@material-ui/icons/LocationOnRounded';
import { useTheme } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
  ${({ variant }) => variant === 'h6' && 'font-size: 16px'};
`;

const Card = styled(MUICard)`
  background-color: ${({ theme }) => theme.colors.paper};
  width: 360px;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      width: 100%;
    }`};
`;

const Location = styled(Button)`
  margin-left: auto;
  &.MuiButton-root {
    .MuiButton-label {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const View = styled(Button)`
  cursor: pointer;
  &.MuiButton-root {
    .MuiButton-label {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const CardHeader = styled(MUICardHeader)`
  &.MuiCardHeader-root {
    .MuiTypography-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const currentProject = {
  label: '210116_BSES_BRPL_SVR_SL',
  started: 'January 16, 2021',
  endDate: 'January 31, 2021',
  location: 'Sarita Vihar Delhi',
  responsible: 'Sanjeev Gupta',
  status: 'Complete'
};

const InfoContainer = styled.div`
  margin-top: 16px;
`;

const CurrentProject = (props) => {
  const theme = useTheme();

  return (
    <Card $theme={theme}>
      <CardActionArea>
        <CardHeader
          avatar={
            <Avatar aria-label="recipe">
              {currentProject.label.slice(0, 1)}
            </Avatar>
          }
          title={currentProject.label}
          subheader={currentProject.started}
        />
        {currentProject.image && (
          <CardMedia
            component="img"
            alt={currentProject.label}
            height="140"
            image={currentProject.image}
            title={currentProject.label}
          />
        )}
        <CardContent>
          {currentProject.description && (
            <Typography variant="body2" component="p">
              {currentProject.description}
            </Typography>
          )}
          <InfoContainer>
            <Typography variant="h6" component="p">
              {currentProject.status}
            </Typography>
            <Typography variant="body2" component="p">
              Status
            </Typography>
          </InfoContainer>
          <InfoContainer>
            <Typography variant="h6" component="p">
              {currentProject.responsible}
            </Typography>
            <Typography variant="body2" component="p">
              Responsible
            </Typography>
          </InfoContainer>
          <InfoContainer>
            <Typography variant="h6" component="p">
              {currentProject.endDate}
            </Typography>
            <Typography variant="body2" component="p">
              End Date
            </Typography>
          </InfoContainer>
        </CardContent>
      </CardActionArea>
      <CardActions disableSpacing>
        <View
          size="small"
          color="primary"
          component={RouterLink}
          to={'/projects/current_project/200829_BRPL_NJF_NNG'}
        >
          View in detail
        </View>
        <Location component="p">
          <LocationOnRoundedIcon />
          {currentProject.location}
        </Location>
      </CardActions>
    </Card>
  );
};

export default CurrentProject;
