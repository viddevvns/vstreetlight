/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Table from 'components/Table';
import { getMaintenanceTask, getTaskHistory } from 'redux/actions/maintenance';
import { useDispatch, useSelector } from 'react-redux';
import Accordion from 'components/Accordion';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-bottom: 40px;
  display: flex;
  flex-wrap: wrap;
`;

const Label = styled(Typography)`
  color: ${({ theme, $health }) => {
    if ($health === 'RED') return '#c62828';
    if ($health === 'YELLOW') return '#ffeb3b';
    if ($health === 'AMBER') return '#ffa000';
    if ($health === 'GREEN') return 'green';
    return theme.colors.baseFont;
  }};
`;

const InfoContainer = styled.div`
  margin-top: 16px;
  flex: 1 1 50%;
`;

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-top: 40px;
  margin-bottom: 30px;
`;

const AssignedTasks = ({ params, user }) => {
  const { lineId } = params;
  const { username } = user;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMaintenanceTask(username));
  }, [dispatch, username]);

  const history = useSelector(({ maintenance }) => maintenance.tasks);

  const onExpansion = (row) => {
    if (!history?.[row['Fault ID']]) dispatch(getTaskHistory(row['Fault ID']));
  };

  const faults = useSelector(({ maintenance }) => maintenance[username]);

  const TaskTable = ({ row }) => {
    const tasks =
      history?.[row['Fault ID']]?.[0]?.['Task Operations Detials']?.[
        'Maintenance Activity'
      ];

    if (!tasks) return null;
    return (
      <Table
        rows={tasks}
        getTo={() => {}}
        getKey={(row, index) => index}
        labels={[
          'Maintenance Activity Type',
          'No of Component replaced',
          'Performed By',
          'Costings',
          'Date',
          'Time'
        ]}
      />
    );
  };

  if (!faults) return null;
  return (
    <>
      <Header>
        <Label variant="h6">Assigned Tasks</Label>
      </Header>
      <Table
        expansion
        onExpansion={onExpansion}
        ExpansionData={TaskTable}
        rows={faults}
        getTo={(row) => `/my_task/${lineId}/assigned_tasks/${row['Fault ID']}`}
        getKey={(row) => row['Fault ID']}
        labels={[
          'Fault ID',
          'Asset ID',
          'Tower ID',
          'Assigned To',
          'Fault Type',
          'Health',
          'Risk Level',
          'Severity',
          'Task Status'
        ]}
      />
    </>
  );
};

export default AssignedTasks;
