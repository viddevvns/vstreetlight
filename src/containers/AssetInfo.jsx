/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const AssetHealth = () => {
  return (
    <>
      <Typography paragraph>Asset Info</Typography>
    </>
  );
};

export default AssetHealth;
