import * as React from 'react';
import { Carousel } from 'react-responsive-carousel';

const SatelliteView = () => {
  return (
    <Carousel autoPlay style = {{ width: "100%", height: "200px" }}>
      <div>
        <img
          alt=""
          src="https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/DElhi_Poorlylit_1.jpg"
          height="100%"
          width="100%"
        />
      </div>
      <div>
        <img
          alt=""
          src="https://akm-img-a-in.tosshub.com/indiatoday/images/story/201910/DIGITAL.png"
          height="100%"
          width="100%"
        />
      </div>
      <div>
        <img
          alt=""
          src="https://akm-img-a-in.tosshub.com/indiatoday/images/story/201910/DIGITAL.png"
          height="100%"
          width="100%"
        />
      </div>
    </Carousel>
  );
};
export default SatelliteView;
