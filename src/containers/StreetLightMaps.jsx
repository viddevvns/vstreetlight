/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Frame = styled.iframe`
  height: calc(100% - 170px);
`;

const StreetLightMaps = () => {
  return (
    <>
      <Typography paragraph>Street Light Maps</Typography>
      <Frame
        src="https://www.google.com/maps/d/u/0/embed?mid=1Bl2DDAfPBFyU_p8tVb5y_2bxrXVLlmjn"
        width="100%"
        title="Street Light Maps"
        frameBorder="0"
      />

      {/*
      <Frame
        src="https://www.google.com/maps/d/embed?mid=1Bl2DDAfPBFyU_p8tVb5y_2bxrXVLlmjn"
        width="100%"
        title="Street Light Maps"
        frameBorder="0"
      />
      */}
    </>
  );
};

export default StreetLightMaps;