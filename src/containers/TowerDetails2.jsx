/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Table from 'components/Table';
import { getTowerInfo } from 'redux/actions/towers';
import { getFaultList, sortFaultDetails } from 'redux/actions/faults';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-bottom: 40px;
  display: flex;
  flex-wrap: wrap;
`;

const Label = styled(Typography)`
  color: ${({ theme, $health }) => {
    if ($health === 'RED') return '#c62828';
    if ($health === 'YELLOW') return '#ffeb3b';
    if ($health === 'AMBER') return '#ffa000';
    if ($health === 'GREEN') return 'green';
    return theme.colors.baseFont;
  }};
`;

const InfoContainer = styled.div`
  margin-top: 16px;
  flex: 1 1 50%;
`;

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-top: 40px;
  margin-bottom: 30px;
`;

const TowerDetails = ({ params }) => {
  const { towerId, lineId } = params;
  const [sortOrder, setSortOrder] = useState('asc');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTowerInfo(towerId));
    dispatch(getFaultList(towerId));
  }, [dispatch, towerId]);

  const details = useSelector(({ towers }) => towers[towerId]);
  const faults = useSelector(({ faults }) => faults[towerId]);

  const handleSort = (key) => {
    const order = sortOrder === 'asc' ? 'desc' : 'asc';
    setSortOrder(order);
    dispatch(sortFaultDetails(towerId, key, order));
  };

  if (!details || !faults) return null;

  return (
    <>
      <Paper>
        {Object.keys(details).map((detail) => (
          <InfoContainer key={detail}>
            <Label $health={details[detail]} variant="h6" component="p">
              {details[detail]}
            </Label>
            <Label variant="body2" component="p">
              {detail}
            </Label>
          </InfoContainer>
        ))}
      </Paper>
      <Header>
        <Label variant="h6">List of Fault Ids</Label>
      </Header>
      <Table
        rows={faults}
        handleSort={handleSort}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Fault ID']}`
        }
        getKey={(row) => row['Fault ID']}
        labels={[
          'Fault ID',
          'Fault Type',
          'Health',
          'Risk Level',
          'Severity',
          'Status'
        ]}
      />
    </>
  );
};

export default TowerDetails;
