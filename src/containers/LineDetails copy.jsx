/* eslint-disable react/prop-types */

import React from 'react';
import Table from 'components/Table';
import { useDispatch, useSelector } from 'react-redux';
import { getLineList } from 'redux/actions/lines';
import { useEffect } from 'react';

const LineDetails = ({ params }) => {
  const { lineId } = params;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getLineList(lineId));
  }, [dispatch, lineId]);

  function createData(ID, Health, Location, Remark, Status, Type) {
    return {
      'Asset ID': ID,
      Health,
      'Provided Asset location': Location,
      Remark,
      Status,
      'Type of Asset': Type
    };
  }

  const rows = useSelector(({ lines }) => lines[lineId]);

  const rows_custom = [
    createData(
      'T0008',
      'RED',
      'Near Keshopur Mandi, Outer Ring Road',
      'Pakka Road (Metal Road)',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0008',
      'RED',
      'Near Keshopur Mandi, Outer Ring Road',
      'Pakka Road (Metal Road),',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0009',
      'RED',
      'Near Keshopur Mandi, Outer Ring Road',
      'Pakka Road (Metal Road),/ Metro',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0010',
      'RED',
      'Near Keshopur Mandi, Outer Ring Road',
      'Pakka Road (Metal Road),/ Metro',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0011',
      'RED',
      'Near Keshopur Mandi, Outer Ring Road',
      'Pakka Road (Metal Road),/ Metro',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0014',
      'RED',
      'Dass Garden, Baprolla',
      'Pakka Road, Drain/ Nala',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0015',
      'RED',
      'Dass Garden, Baprolla',
      'Pakka Road',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0016',
      'RED',
      'Dass Garden, Baprolla',
      'Pakka Road',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0023',
      'RED',
      'Vikas Nagar, Hastal, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0024',
      'RED',
      'Vikas Nagar, Hastal, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0025',
      'RED',
      'Vikas Nagar, Hastal, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0026',
      'RED',
      'Vikas Nagar, Hastal, Vikaspuri',
      'School',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0033',
      'RED',
      'Shiv Vihar, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0034',
      'RED',
      'Shiv Vihar, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0035',
      'RED',
      'Shiv Vihar, Vikaspuri',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0036',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0037',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0038',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0039',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0040',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0041',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Park',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0042',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0043',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0044',
      'RED',
      'Kamruddin Nagar, Near NWW Grid',
      'Temple',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0046',
      'RED',
      'Laxmi vihar, Vandana Vihar, Nangloi',
      'Gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0047',
      'RED',
      'Laxmi vihar, Vandana Vihar, Nangloi',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0048',
      'RED',
      'Laxmi vihar, Vandana Vihar, Nangloi',
      'Pakka',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0049',
      'RED',
      'Laxmi vihar, Vandana Vihar, Nangloi',
      'Gali',
      'NOT SCHEDULED',
      'LED'
    ),
    createData(
      'SL0050',
      'RED',
      'Laxmi vihar, Vandana Vihar, Nangloi',
      'off road',
      'NOT SCHEDULED',
      'LED'
    )
  ];
  if (!rows) return null;
  return (
    <Table
      rows={rows}
      getTo={(row) =>
        '/asset_health/asset_details/Najafgar line Between Najafgarh and Nanglor 2- BTL 27/' +
        row['Asset ID']
      }
      getKey={(row) => row['Asset ID']}
      labels={[
        'Asset ID',
        'Health',
        'Provided Asset location',
        'Remark',
        'Status',
        'Type of Asset'
      ]}
    />
  );
};

export default LineDetails;
