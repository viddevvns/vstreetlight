/* eslint-disable react/prop-types */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Table from 'components/Table';

function createData(Asset, Available, Sleeve, Broken, Hanging, Risk, Health) {
  return {
    Asset,
    Available,
    Sleeve,
    Broken,
    Hanging,
    Risk,
    Health
  };
}

const rows = [
  createData('7 to 8', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('8 to 9', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('9 to 10', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('10 to 11', '1', '0', 'N/A', 'N/A', '1', 'Green'),
  createData('13 to 14', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('14 to 15', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('15 to 16', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('22 to 23', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('23 to 24', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('24 to 25', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('25 to 26', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('32 to 33', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('33 to 34', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('34 to 35', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('35 to 36', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('36 to 37', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('37 to 38', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('38 to 39', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('39 to 40', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('40 to 41', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('41 to 42', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('42 to 43', '1', '0', 'Broken', 'Hanging', '1', 'Green'),
  createData('43 to 44', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('45 to 46', '0', '0', 'Broken', 'Hanging', '5', 'Red'),
  createData('46 to 47', '0', '0', 'N/A', 'N/A', '5', 'Red'),
  createData('47 to 48', '1', '0', 'N/A', 'N/A', '1', 'Green'),
  createData('48 to 49', '1', '0', 'N/A', 'N/A', '1', 'Green'),
  createData('49 to 50', '1', '0', 'N/A', 'N/A', '1', 'Green')
];

const Conductor = () => {
  return (
    <Table
      rows={rows}
      getTo={(row) => ''}
      getKey={(row, index) => index}
      labels={[
        'Asset',
        'Available',
        'Sleeve',
        'Broken',
        'Hanging',
        'Risk',
        'Health'
      ]}
    />
  );
};

export default Conductor;
