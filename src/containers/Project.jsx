/* eslint-disable react/prop-types */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import MUILinearProgress from '@material-ui/core/LinearProgress';
import Table from 'components/Table';
import { Link as Routerlink } from 'react-router-dom';

function createData(user, Department, Role, Location, Contact) {
  return { 'User ID': user, Department, Role, Location, Contact };
}

const rows = [
  createData(
    'Sanjeev Gupta',
    'Head of Project',
    'Senior Vice President',
    'Delhi',
    98000989
  ),
  createData(
    'Vinod.B.Sharma',
    'Street Light',
    'Head of Meter Management',
    'Delhi',
    98000989
  ),
  createData(
    'Pushpdeep Jaisiya',
    'B.M',
    'Senior Manager',
    'Delhi',
    98000989
  ),
  createData(
    'Vinay.R.Kumar',
    'Street Light',
    '',
    'Delhi',
    98000989
  ),
  createData(
    'Kashmir.Singh',
    'Street Light',
    '',
    'Delhi',
    98000989
  ),
  createData(
    'Pramod.Mishra',
    'Street Light',
    '',
    'Delhi',
    98000989
  ),
  createData(
    'Abhishek Ranjan',
    'AVP Management',
    'Project Owner',
    'Delhi',
    98000989
  ),
  createData(
    'Avinash Kumar',
    'Project Manager',
    'EHV Manager',
    'Delhi',
    98000989
  ),
  createData('Mudgal', 'Line Manager', 'EHV Manager', 'Delhi', 98000989),
  createData('Puneet', 'Line Incharge', 'EHV Team', 'Sarita Vihar', 98000989),
  createData('Lineman1', 'Line man', 'EHV Team', 'Sarita Vihar', 98000989)
];

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-bottom: 40px;
  display: flex;
  flex-wrap: wrap;
`;

const Label = styled(Typography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const LinearProgress = styled(MUILinearProgress)`
  margin: 14px 0;
`;

const InfoContainer = styled.div`
  margin-top: 16px;
  flex: 1 1 50%;
`;

const StyledProgress = styled(LinearProgress)`
  &.MuiLinearProgress-root {
    height: 8px;
    background-color: #08bd80;
    align-self: center;
    border-radius: 8px;
  }
  &.MuiLinearProgress-root .MuiLinearProgress-bar {
    border-radius: 8px;
    background-color: #08bd80;
  }
`;

const Link = styled(Routerlink)`
  color: ${({ theme }) => theme.colors.baseFont};
  &:hover {
    text-decoration: underline;
  }
`;

const Project = ({ params }) => {
  const { projectId } = params;
  const currentProjectDetail = {
    'Project Name': '200801_BSES_BRPL_SVR',
    'Project ID': '200829_BRPL_SVR',
    'Project Asset Owner': '200829_BRPL_SVR',
    'Project Line': 'Sarita Vihar',
    Assets: '62 Street Lights',
    Location: 'Delhi, India',
    'Project GNSS': (
      <Link to={`/projects/current_project/${projectId}/GNSS`}>GNSS</Link>
    ),
    'Start Date': 'August 1, 2020',
    'End Date': 'September 1, 2020',
    'Project Last Ispected': 'September 5, 2020',
    'Project Phase': 'Data Analytics',
    'Project Progress bar': undefined
  };
  return (
    <div style={{ width: '100%' }}>
      <Paper>
        {Object.keys(currentProjectDetail).map((detail) => (
          <InfoContainer key={detail}>
            {currentProjectDetail[detail] ? (
              <Label variant="h6" component="p">
                {currentProjectDetail[detail]}
              </Label>
            ) : (
              <StyledProgress variant="determinate" value={100} />
            )}
            <Label variant="body2" component="p">
              {detail}
            </Label>
          </InfoContainer>
        ))}
      </Paper>

      <Table
        rows={rows}
        getTo={(row) => ''}
        getKey={(row) => {
          return row['User ID'];
        }}
        labels={['User ID', 'Department', 'Role', 'Location', 'Contact']}
      />
    </div>
  );
};

export default Project;
