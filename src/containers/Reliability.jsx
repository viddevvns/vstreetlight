import * as React from 'react';
import styled from 'styled-components';
import Card from 'components/Reliability/Card';
import Slider from '@material-ui/core/Slider';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import GDAP from 'data/GraphDataAssetPerformance';
import GDR from 'data/GraphDataReliability';
import MyResponsiveAreaBump from 'components/NivoCharts/MyResponsiveAreaBump';
import MyResponsivePie from 'components/NivoCharts/MyResponsivePie';
import MyResponsiveLine from 'components/NivoCharts/MyResponsiveLine';
import MyResponsiveBar from 'components/NivoCharts/MyResponsiveBar';

const H1 = styled.h1`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const H2 = styled.h2`
  color: ${({ theme }) => theme.colors.baseFont};
  text-align: center;
`;

const P = styled.p`
  margin: ${({ $noMargin }) => ($noMargin ? '0' : '0 0 0 8px')};
  color: ${({ theme }) => theme.colors.baseFont};
`;
const Edit = styled.button`
  color: #fff;
  border: 1px solid #5670fb;
  background-color: #5670fb;
  padding: 7px 14px;
  border-radius: 24px;
  cursor: pointer;
  margin-top: 24px;
  outline: none;
`;

const Grid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: ${({ gap }) => gap || '32px'};
  row-gap: ${({ gap }) => gap || '32px'};
  @media (max-width: 1100px) {
    grid-template-columns: 1fr;
  }
`;

const Wrapper = styled(Grid)`
  @media (max-width: 1250px) {
    grid-template-columns: 1fr;
  }
`;

const Grid3 = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  column-gap: ${({ gap }) => gap || '28px'};
  row-gap: ${({ gap }) => gap || '28px'};
  @media (max-width: 1300px) {
    grid-template-columns: 1fr 1fr;
  }
`;

const Grid2 = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: ${({ gap }) => gap || '28px'};
  row-gap: ${({ gap }) => gap || '28px'};
  @media (max-width: 1300px) {
    grid-template-columns: 1fr 1fr;
  }
`;

const Flex = styled.div`
  display: flex;
  flex-direction: ${({ $dir }) => $dir || 'row'}; ;
`;

const BgCard = styled(Card)`
  margin-top: 64px;
  background-color: ${({ theme }) => theme.colors.border};
`;

const GraphCard = styled(Card)`
  max-height: 256px;
  margin: -10px;
  margin-bottom: 20px;

`;

const Input = styled.input`
  outline: none;
  height: 36px;
  border: none;
  background: transparent;
  border-bottom: 1px solid gray;
  color: ${({ theme }) => theme.colors.baseFont};
`;

const initialDivisionPanel = [
  {
    key: 'Division',
    value: 'Saritha Vihar Division'
  },
  {
    key: 'Month',
    value: 2021
  },
  {
    key: 'Number of Street Light',
    value: 62
  },
  {
    key: 'Functional ON | OFF ',
    value: '51 | 11'
  },
  {
    key: 'Repairs in a month',
    value: 17
  },
  {
    key: 'Replacement in a month',
    value: 4
  },
  {
    key: 'Faults Detected in a month',
    value: 22
  }
];

const initialOutputPanel = [
  {
    key: 'Mean Time To Repair (MTTR)',
    value: 6
  },
  {
    key: 'Mean Time To Recover (MTTR)',
    value: 3
  },
  {
    key: 'Mean Time To Respond (MTTR)',
    value: 7
  },
  {
    key: 'Mean Time To Replace (MTTR)',
    value: 5
  },
  {
    key: 'Mean Time To Fault (MTTF)',
    value: 17
  },
  {
    key: 'Mean Time Between Failures',
    value: 45
  },
  {
    key: 'Avaibility (A) %',
    value: 83
  },
  {
    key: 'Performance (P) %',
    value: 90
  },
  {
    key: 'Quality (Q)',
    value: 'A'
  },
  {
    key: 'Effectiveloss (EL) %',
    value: 3
  },
  {
    key: 'Relaibility (R) %',
    value: 98
  }  
];



const Reliability = () => {
  const [divisionPanel, setDivisionPanel] = React.useState(
    () => initialDivisionPanel
  );
  const [simulationPanel, setSimulationPanel] = React.useState({});
  const [predictorsPanel, setPredictorsPanel] = React.useState(
    () => initialDivisionPanel
  );
  const [ouputPanel, setOutputPanel] = React.useState(
    () => initialOutputPanel
  );
  const [formats, setFormats] = React.useState(() => ['Laboratory', 'Physical']);
  const handleFormat = (event, newFormats) => {
    setFormats(newFormats);
  };
  function updateOutput(val) {
   var calOutputPanel = initialOutputPanel;
   calOutputPanel[0]['value'] += calOutputPanel[0]['value']*val/1000;
   calOutputPanel[1]['value'] += calOutputPanel[1]['value']*val/1000;
   calOutputPanel[2]['value'] += calOutputPanel[2]['value']*val/1000;
   calOutputPanel[3]['value'] += calOutputPanel[3]['value']*val/1000;
   calOutputPanel[4]['value'] -= calOutputPanel[4]['value']*val/10000;
   calOutputPanel[5]['value'] -= calOutputPanel[5]['value']*val/10000;
   calOutputPanel[6]['value'] -= calOutputPanel[6]['value']*val/10000;
   calOutputPanel[7]['value'] -= calOutputPanel[7]['value']*val/10000;
   calOutputPanel[9]['value'] += calOutputPanel[9]['value']*val/10000;
   calOutputPanel[10]['value'] -= calOutputPanel[10]['value']*val/10000;
   {/*console.log(calOutputPanel);*/}
   setOutputPanel(calOutputPanel);
   console.log(ouputPanel);
  }
  return (
    <>
      <H1>Reliability Simulator</H1>
      <Grid>
        <Card title="Division Details Panel">
          <Wrapper gap="16px">
            {divisionPanel.map((panel, index) => (
              <Flex key={String(index)}>
                <P>{`${panel.key} :`}</P>
                <P>{panel.value}</P>
              </Flex>
            ))}
          </Wrapper>
        </Card>
        <Card title="Simulation Panel">
          <Wrapper gap="16px">
            <Flex $dir="column">
              <P $noMargin>Months Range</P>
              <Input placeholder="10" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Number of Street Lights</P>
              <Input placeholder="62" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Average Functional ON | OFF</P>
              <Input placeholder="51 | 11" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Average Repairs in a month</P>
              <Input placeholder="17" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Average Replacement in a month</P>
              <Input placeholder="4" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Average Faults Detected in a month</P>
              <Input placeholder="22" />
            </Flex>
          </Wrapper>
          <Edit>Calculate</Edit>
        </Card>
        <Card title="Predictors Panel">
          <Wrapper gap="16px">
            <Flex $dir="column">
              <P $noMargin>Predict for months</P>
              <Input placeholder="10" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Expected Quality Grade</P>
              <Input placeholder="1 - 10" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Model Accuracy (%)</P>
              <Input placeholder="81" />
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Evironment</P>
              <ToggleButtonGroup value={formats} onChange={handleFormat} aria-label="text formatting">
                <ToggleButton value="Laboratory" aria-label="bold">
                  Laboratory
                </ToggleButton>
                <ToggleButton value="Physical" aria-label="italic">
                  Physical
                </ToggleButton>
              </ToggleButtonGroup>
            </Flex>
            <Flex $dir="column">
              <P $noMargin>Weather Exposure Scale</P>
              <Slider
                defaultValue={30}
                onChange={ (e, val) => updateOutput(val) }  
                getAriaValueText={(value) => `${value}°C`}
                aria-labelledby="discrete-slider"
                valueLabelDisplay="auto"
                step={10}
                marks
                min={10}
                max={100}
              />
            </Flex>
          </Wrapper>
          <Edit>Calculate</Edit>
        </Card>
        <Card title="Reliability Panel">
          <Wrapper gap="16px">
            {ouputPanel.map((panel, index) => (
              <Flex key={String(index)}>
                <P>{`${panel.key} :`}</P>
                <P>{panel.value}</P>
              </Flex>
            ))}
          </Wrapper>
        </Card>
      </Grid>
      <BgCard>
        <Grid3>
          <GraphCard>
            <MyResponsiveAreaBump data={GDR["Energy and Power Analysis"]}/>
            <H2>Energy and Power Analysis</H2>
          </GraphCard>
          <GraphCard>
            <MyResponsiveBar data={GDAP.bar} data={GDR['Timeseries Analysis']} keys={['Performance (contribution to system, work done - time invested )', 'Failure Rate', 'Reliability', 'Availability']}  indexBy={'Timeline(days)'} legendx={'Time series'} legendy={'Matrices'} groupMode={"grouped"}/>
            <H2>Timeseries Analysis</H2>
          </GraphCard>
          <GraphCard>
            <MyResponsiveLine data={GDR["conceptVizs"]}/>
            <H2>MTTRs, MTTF and MTBF Viz</H2>
          </GraphCard>
        </Grid3>
      </BgCard>
      <BgCard>
        <Grid2>
          <GraphCard>
            <MyResponsivePie data={GDR["durability_pie"]}/>
            <H2>Average Durability of Asset Components</H2>
          </GraphCard>
          <GraphCard>
            <MyResponsiveBar data={GDR['Reliability (for next n days) of Assets']} keys={['Power Transformer', 'SF6 circuit breaker', 'transmission', 'line/cable', 'protection system']} indexBy={'Asset'} legendx={'Assets'} legendy={'Reliability'} groupMode={"stacked"}/>
            <H2>Assets Reliability Scores</H2>
          </GraphCard>
          <GraphCard>
            <MyResponsiveAreaBump data={GDR["Energy and Power Analysis"]}/>
            <H2>Avaibility of Lines Yearly</H2>
          </GraphCard>
          <GraphCard>
            <MyResponsiveLine data={GDR["conceptVizs"]}/>
            <H2>Effective Losses Monthly</H2>
          </GraphCard>
        </Grid2>
      </BgCard>
    </>
  );
};

export default Reliability;
