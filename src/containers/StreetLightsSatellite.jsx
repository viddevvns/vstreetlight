/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Frame = styled.iframe`
  height: calc(100% - 170px);
`;

const StreetLightsSatellite = () => {
  return (
    <>
      <Typography paragraph>
        {'Street Lights Satellite Monitoring Grid System'}
      </Typography>
      <Frame
        src="https://datatrip.ml/map.html?l=newdelhi"
        width="100%"
        title="Street Light Satellite MonGrid System"
        frameBorder="0"
      />
    </>
  );
};

export default StreetLightsSatellite;
