/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import { assetGraphs } from 'utils/constants';
import GraphFrame from 'components/GraphFrame';
import LineGraph from 'components/LineGraph';
import { useTheme } from '@material-ui/core/styles';
import { getAnalyticDetails } from 'redux/actions/analytics';
import BarGraph from 'components/BarGraph';
import PieGraph from 'components/PieGraph';
import GDAP from 'data/GraphDataAssetPerformance';

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 25px 0;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      font-size: 18px;
    }`};
`;

const Container = styled.div`
  text-align: center;
  width: 100%;
`;

const GraphsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-top: 20px;
  gap: 20px;
`;

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: 80px;
  padding: 0 5%;
  width: 100%;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      padding: 0;
    }`};
`;

const AssetsPerformance = () => {
  const theme = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAnalyticDetails());
  }, [dispatch]);

  const analytics = useSelector(({ analytics }) => analytics);

  const grphDataOne = [
    {
      title: 'Maintenance Task Status vs Severity',
      data: analytics.haAssetNaturalCalamitiesAnl,
      type: 'bar'
    }
  ];
  const power_switch = [
    { name: 'Turned On', value: 10 },
    { name: 'Turned Off', value: 14 }
  ];
  const power_consumption = [
    { name: 'Turned On Power Loss', value: 2 },
    { name: 'Turned Off Power Loss', value: 22 }
  ];
  const grphDataZero = [
    {
      title: 'Street Lights Switch',
      data: power_switch,
      data1: power_consumption,
      type: 'pie'
    },
    {
      title: 'Street Lights Power Comsumption',
      data: analytics.haAssetCountFaultsAnl,
      type: 'line'
    },
    {
      title: 'Street Lights Power Availabilty',
      data: analytics.haTotalFaultsTimeAnl,
      type: 'line'
    },
    {
      title: 'Maintenance Task Status vs Severity',
      data: analytics.haAssetNaturalCalamitiesAnl,
      type: 'bar'
    }
  ];
  console.log(GDAP);
  return (
    <Root $theme={theme}>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {assetGraphs.title}
          </Typography>
        </Header>
        <GraphsContainer>
          {/* {assetGraphs.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))} */}
          {grphDataZero.map((graph) => (
            <GraphFrame key={graph.title} title={graph.title}>
              {graph.type === 'bar' && <BarGraph graph={graph} />}
              {graph.type === 'line' && <LineGraph graph={graph} />}
              {graph.type === 'pie' && <PieGraph graph={graph} />}
            </GraphFrame>
          ))}
        </GraphsContainer>
      </Container>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {'Asset Maintenance Analysis'}
          </Typography>
        </Header>
        <GraphsContainer>
          {/* {assetGraphs.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))} 
          {grphDataOne.map((graph) => (
            <GraphFrame key={graph.title} title={graph.title}>
              {graph.type === 'bar' && <BarGraph graph={graph} />}
              {graph.type === 'line' && <LineGraph graph={graph} />}
              {graph.type === 'pie' && <PieGraph graph={graph} />}
            </GraphFrame>
          ))}*/}
        </GraphsContainer>
        <GraphsContainer>
          {analytics?.haAssetHealthAnl?.map((tower) => (
            <GraphFrame key={tower.title} title={tower.name}>
              <BarGraph graph={tower} x="category" />
            </GraphFrame>
          ))}
        </GraphsContainer>
      </Container>
    </Root>
  );
};

export default AssetsPerformance;
