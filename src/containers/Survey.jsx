/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Survey = () => {
  return (
    <>
      <Typography paragraph>Survey</Typography>
    </>
  );
};

export default Survey;
