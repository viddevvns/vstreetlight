/* eslint-disable react/prop-types */
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MUITypography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import MUITextField from '@material-ui/core/TextField';
import MUIButton from '@material-ui/core/Button';
import styled from 'styled-components';

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const TextField = styled(MUITextField)`
  margin-bottom: 20px;
`;

const Button = styled(MUIButton)``;

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    width: 400
  },
  superroot: {
  }
}));

export default function Home({ handleLogin, user }) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  if (user) return <Typography>Home</Typography>;

  return (
    <div className={classes.superroot}>
      <div className={classes.root}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab label="Login" {...a11yProps(0)} />
            <Tab disabled label="Signup" {...a11yProps(1)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Form onSubmit={handleLogin} noValidate autoComplete="off">
            <TextField name="email" label="Username" />
            <TextField name="password" label="Password" type="password" />
            <Button type="submit" variant="contained">
              Login
            </Button>
          </Form>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Form noValidate autoComplete="off">
            <TextField id="standard-basic" label="Username" />
            <TextField id="standard-basic" label="Password" type="password" />
            <Button type="submit" variant="contained">
              Signup
            </Button>
          </Form>
        </TabPanel>
      </div>
    </div>
  );
}
