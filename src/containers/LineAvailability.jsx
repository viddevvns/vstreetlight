/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import { lineAvailabilityGraphs } from 'utils/constants';
import GraphFrame from 'components/GraphFrame';
import { useTheme } from '@material-ui/core/styles';

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 25px 0;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      font-size: 18px;
    }`};
`;

const Container = styled.div`
  text-align: center;
  width: 100%;
`;

const GraphsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-top: 20px;
  gap: 20px;
`;

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: 80px;
  padding: 0 5%;
  width: 100%;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      padding: 0;
    }`};
`;

const LineAvailability = () => {
  const theme = useTheme();

  return (
    <Root $theme={theme}>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {lineAvailabilityGraphs.title}
          </Typography>
        </Header>
        <GraphsContainer>
          {lineAvailabilityGraphs.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))}
        </GraphsContainer>
      </Container>
    </Root>
  );
};

export default LineAvailability;
