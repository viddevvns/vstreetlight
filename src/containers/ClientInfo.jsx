/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
// import MUIAvatar from '@material-ui/core/Avatar';
import { useTheme } from '@material-ui/core/styles';
// import EditRoundedIcon from '@material-ui/icons/EditRounded';
// import MUIIconButton from '@material-ui/core/IconButton';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 40px;
  width: 100%;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      width: 100%;
    }`};
`;

// const AvatarPaper = styled(MUIPaper)`
//   background-color: ${({ theme }) => theme.colors.paper};
//   display: flex;
//   width: 38%;
//   ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
//       width: 100%;
//     }`};
// `;

// const Avatar = styled(MUIAvatar)`
//   background-color: #ff5722;
//   margin: auto;
//   width: ${({ $theme }) => $theme.spacing(14) + 'px'};
//   height: ${({ $theme }) => $theme.spacing(14) + 'px'};
// `;

// const IconButton = styled(MUIIconButton)`
//   color: ${({ theme }) => theme.colors.baseFont};
//   margin: auto;
//   display: block;
// `;

const InfoContainer = styled.div`
  margin-top: 16px;
`;

const ClientInfo = ({ user }) => {
  const theme = useTheme();

  return (
    <>
      {/* <AvatarPaper $theme={theme}>
        <Avatar $theme={theme} />
        <IconButton>
          <EditRoundedIcon />
        </IconButton>
      </AvatarPaper> */}
      <Paper $theme={theme}>
        <InfoContainer>
          <Typography variant="h6" component="p">
            {user.email_id}
          </Typography>
          <Typography variant="body2" component="p">
            Email
          </Typography>
        </InfoContainer>
        <InfoContainer>
          <Typography variant="h6" component="p">
            {user.username}
          </Typography>
          <Typography variant="body2" component="p">
            Username
          </Typography>
        </InfoContainer>
        <InfoContainer>
          <Typography variant="h6" component="p">
            {user.access_role}
          </Typography>
          <Typography variant="body2" component="p">
            Access Role
          </Typography>
        </InfoContainer>
      </Paper>
    </>
  );
};

export default ClientInfo;
