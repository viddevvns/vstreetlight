/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Table from 'components/Table';
import { getTowerInfo } from 'redux/actions/towers';
import { getFaultList } from 'redux/actions/faults';
import { getLineList } from 'redux/actions/lines';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-bottom: 40px;
  display: flex;
  flex-wrap: wrap;
`;

const Label = styled(Typography)`
  color: ${({ theme, $health }) => {
    if ($health === 'RED') return '#c62828';
    if ($health === ' RED') return '#c62828';
    if ($health === 'YELLOW') return '#ffeb3b';
    if ($health === 'AMBER') return '#ffa000';
    if ($health === 'GREEN') return 'green';
    return theme.colors.baseFont;
  }};
`;

const InfoContainer = styled.div`
  margin-top: 16px;
  flex: 1 1 33%;
`;

const InfoSubDivContainer = styled.div`
  margin-top: 12px;
  flex: 1 1 50%;
`;

const InfoDivContainer = styled.div`
  margin-top: 14px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: ${({ gap }) => gap || '22px'};
  row-gap: ${({ gap }) => gap || '12px'};
  @media (max-width: 1100px) {
    grid-template-columns: 1fr;
`;

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-top: 40px;
  margin-bottom: 20px;
`;
const TowerDetails = ({ params }) => {
  const { towerId, lineId } = params;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getTowerInfo(towerId));
    dispatch(getFaultList(towerId));
  }, [dispatch, towerId]);
  const details = useSelector(({ towers }) => towers[towerId]);
  function createData(ID, Date, Time, Activity, Responsible) {
    return {
      'Activity ID': ID + towerId,
      'Asset ID': towerId,
      Date,
      Time,
      Activity,
      Responsible
    };
  }
  function createMaintenanceData(ID, Type, Health, Risk, Severity, Status) {
    return {
      'Maintenance ID': ID + towerId,
      Type,
      Health,
      'Risk Level': Risk,
      Severity,
      Status
    };
  }
  function createFaultsData(ID, Type, Action, Health, Risk, Severity, Status) {
    return {
      'Fault ID': ID + towerId,
      Type,
      'Action Req': Action,
      Health,
      'Risk Level': Risk,
      Severity,
      Status
    };
  }
  const rows_maintenance = [
    createMaintenanceData(
      '2021-02-16_M_',
      'Inspection',
      'Green',
      'Asset Health',
      '1',
      'Completed'
    ),
    createMaintenanceData(
      '2021-03-16_M_',
      'Inspection',
      'Amber',
      'Asset Health',
      '2',
      'Scheduled'
    ),
    createMaintenanceData(
      '2021-04-16_M_',
      'Inspection',
      'Amber',
      'Asset Health',
      '2',
      'Not Scheduled'
    ),
    createMaintenanceData(
      '2021-05-16_M_',
      'Inspection',
      'Green',
      'Asset Health',
      '1',
      'Not Scheduled'
    )
  ];
  const rows_activites = [
    createData('2021-01-16T05:30_A_', '16/01/21', '5:32', 'Turned Off', 'Admin'),
    createData('2021-01-16T05:30_A_', '16/01/21', '18:23', 'Turned On', 'Admin'),
    createData('2021-01-16T05:30_A_', '17/01/21', '5:23', 'Turned Off', 'Admin'),
    createData('2021-01-16T05:30_A_', '17/01/21', '18:44', 'Turned On', 'Admin')
  ];

  const rows_faults = [
    createFaultsData(
      '2021-02-16_F_',
      'LED Fused',
      'Replace',
      'RED',
      'Asset Health & Life',
      '5',
      'Not Scheduled'
    ),
    createFaultsData(
      '2021-03-16_F_',
      'Pole Bend',
      'Repair',
      'Amber',
      'Asset Health',
      '2',
      'Not Scheduled'
    ),
    createFaultsData(
      '2021-04-16_F_',
      'Lamp Cover',
      'Cleaning',
      'Green',
      'Asset Health',
      '2',
      'Not Scheduled'
    ),
    createFaultsData(
      '2021-05-16_F_',
      'Heating',
      'Inspection',
      'Green',
      'Asset Health',
      '1',
      'Scheduled'
    )
  ];
  const card1 = ['Pole Number', 'Health', 'Provided Asset location', 'Remark', 'Status', 'Type of Asset', 'Street Name', 'Asset Company', 'Previous Inspection', 'Datte of Instalation (Age)'];
  const card2 = ['Arm Length', 'Arm Style', 'Pole Height', 'Pole Composition', 'Power Feed', 'Color Temperature(CCT)'];
  const card3 = ['Is Metered', 'Light Manufacturer', 'IP Rating', 'Lifespan', 'Proximity', 'Quadrant', 'Shield', 'Base Type'];

  if (!details) return null;
  return (
    <>
      <Label variant="h6" style = {{ textAlign: 'center', fontStyle: 'italic' }}>Generic Information</Label>
      <Paper style = {{ marginTop: '14px'}}>
        {Object.keys(details).filter(key => card1.includes(key)).map((detail) => (
          <InfoContainer key={detail}>
            <Label $health={details[detail]} variant="h6" component="p">
              {details[detail]}
            </Label>
            <Label variant="body2" component="p">
              {detail}
            </Label>
          </InfoContainer>
        ))}
      </Paper>
      <InfoDivContainer >
        <InfoSubDivContainer style = {{ textAlign: 'center', justifyContent: 'center', marginTop: '-8px' }}>
          <Label variant="h6" style = {{ textAlign: 'center', fontStyle: 'italic' }}>Component - Specifications</Label>
        </InfoSubDivContainer>
        <InfoSubDivContainer style = {{ textAlign: 'center', justifyContent: 'center', marginTop: '-8px' }}>
          <Label variant="h6" style = {{ textAlign: 'center', fontStyle: 'italic' }}>Operational - Specifications</Label>
        </InfoSubDivContainer>
      </InfoDivContainer>
      <InfoDivContainer>
        <Paper>
          {Object.keys(details).filter(key => card2.includes(key)).map((detail) => (
            <InfoSubDivContainer key={detail}>
              <Label $health={details[detail]} variant="h6" component="p">
                {details[detail]}
              </Label>
              <Label variant="body2" component="p">
                {detail}
              </Label>
            </InfoSubDivContainer>
          ))}
        </Paper>
        <Paper>
          {Object.keys(details).filter(key => card3.includes(key)).map((detail) => (
            <InfoSubDivContainer key={detail}>
              <Label $health={details[detail]} variant="h6" component="p">
                {details[detail]}
              </Label>
              <Label variant="body2" component="p">
                {detail}
              </Label>
            </InfoSubDivContainer>
          ))}
        </Paper>
      </InfoDivContainer>
      <Header>
        <Label variant="h6">List of Fault IDs</Label>
      </Header>
      <Table
        rows={rows_faults}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Fault ID']}`
        }
        getKey={(row) => row['Fault ID']}
        labels={[
          'Fault ID',
          'Type',
          'Action Req',
          'Health',
          'Risk Level',
          'Severity',
          'Status'
        ]}
      />
      <Header>
        <Label variant="h6">Maintenance Records</Label>
      </Header>
      <Table
        rows={rows_maintenance}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Maintenance ID']}`
        }
        getKey={(row) => row['Maintenance ID']}
        labels={[
          'Maintenance ID',
          'Type',
          'Health',
          'Risk Level',
          'Severity',
          'Status'
        ]}
      />
      <Header>
        <Label variant="h6">CCMS Monitoring Activities</Label>
      </Header>
      <Table
        rows={rows_activites}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Asset ID']}`
        }
        getKey={(row) => row['Activity ID']}
        labels={[
          'Activity ID',
          'Asset ID',
          'Date',
          'Time',
          'Activity',
          'Responsible'
        ]}
      />
    </>
  );
};

export default TowerDetails;
