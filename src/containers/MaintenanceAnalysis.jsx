/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import GraphFrame from 'components/GraphFrame';
import BarGraph from 'components/BarGraph';
import PieGraph from 'components/PieGraph';
import { graphsOne, graphsTwo, graphsThree } from 'utils/constants';
import { useTheme } from '@material-ui/core/styles';
import { getAnalyticDetails } from 'redux/actions/analytics';

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 25px 0;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      font-size: 18px;
    }`};
`;

const Container = styled.div`
  text-align: center;
  width: 100%;
`;

const GraphsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-top: 20px;
  gap: 20px;
`;

const Root = styled.div`
  display: flex;
  flex-direction: column;
  gap: 80px;
  padding: 0 5%;
  width: 100%;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      padding: 0;
    }`};
`;

const MaintenanceAnalysis = () => {
  const theme = useTheme();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAnalyticDetails());
  }, [dispatch]);

  const analytics = useSelector(({ analytics }) => analytics);

  const grphDataOne = [
    {
      title: 'All Task Status vs Severity',
      data: analytics.mAllTaskStatusvsSeverityAnl,
      type: 'bar'
    },
    {
      title: 'Completed Task Status vs Severity',
      data: analytics.mCompletedTaskStatusvsSeverityAnl,
      type: 'bar'
    },
    {
      title: 'Task Status vs Count',
      data: analytics.mTaskStatusvsCountAnl,
      type: 'bar'
    },
    {
      title: 'Not Scheduled Tasks with Task Severity',
      data: analytics.mNotScheduledTaskStatusvsSeverityAnl,
      type: 'bar'
    },
    {
      title: 'Scheduled Tasks with Tasks Severity',
      data: analytics.mScheduledTaskStatusvsSeverityAnl,
      type: 'bar'
    },
    {
      title: 'Need Approval Tasks Status vs Severity',
      data: analytics.mNeedApprovalTaskStatusvsSeverityAnl,
      type: 'bar'
    }
  ];

  const graphDataTwo = [
    {
      title: 'Weekly Estimate and Actual Costings',
      data: analytics.mWeeklyEstimateNActualCostingsAnl,
      type: 'bar'
    },
    {
      title: 'Actual Costing VS Task Status',
      data: analytics.mActualCostingVSTaskStatusAnl,
      type: 'pie'
    },
    {
      title: 'Estimated Costing VS Task Status',
      data: analytics.mEstimatedCostingVSTaskStatusAnl,
      type: 'pie'
    },
    {
      title: 'Tasks Assigned Personnel',
      data: analytics.mTasksAssignedPersonnelAnl,
      type: 'pie'
    },
    {
      title: 'Count Task Status PerAssigned Personnel',
      data: analytics.mCountTaskStatusPerAssignedPersonnelAnl,
      type: 'bar'
    }
  ];

  console.log(analytics);

  return (
    <Root $theme={theme}>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {graphsOne.title}
          </Typography>
        </Header>
        <GraphsContainer>
          {/* {graphsOne.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))} */}
          {grphDataOne.map((graph) => (
            <GraphFrame key={graph.title} title={graph.title}>
              {graph.type === 'bar' && <BarGraph graph={graph} />}
              {graph.type === 'pie' && <PieGraph graph={graph} />}
            </GraphFrame>
          ))}
        </GraphsContainer>
      </Container>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {graphsTwo.title}
          </Typography>
        </Header>
        <GraphsContainer>
          {/* {graphsTwo.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))} */}
          {graphDataTwo.map((graph) => (
            <GraphFrame key={graph.title} title={graph.title}>
              {graph.type === 'bar' && <BarGraph graph={graph} />}
              {graph.type === 'pie' && <PieGraph graph={graph} />}
            </GraphFrame>
          ))}
        </GraphsContainer>
      </Container>
      <Container>
        <Header>
          <Typography $theme={theme} variant="h4">
            {graphsThree.title}
          </Typography>
        </Header>
        <GraphsContainer>
          {/* {graphsThree.graphs.map((graph) => (
            <GraphFrame key={graph.title} src={graph.src} title={graph.title} />
          ))} */}
        </GraphsContainer>
      </Container>
    </Root>
  );
};

export default MaintenanceAnalysis;
