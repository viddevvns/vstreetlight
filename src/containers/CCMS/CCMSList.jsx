/* eslint-disable react/prop-types */

import React from 'react';
import Table from 'components/Table';
import { useDispatch, useSelector } from 'react-redux';
import { getLineList } from 'redux/actions/lines';
import { useEffect } from 'react';
import ccms_svr from 'data/CCMS_SVR';

const CCMSList = ({ params }) => {
  {/*const { lineId } = params;
    const dispatch = useDispatch();
  
    useEffect(() => {
      dispatch(getLineList(lineId));
    }, [dispatch, lineId]);*/}

  function createData(Circle, ContractAccount, Division, Address, Meter, CCMSMtrNo, CCMSPanelNo) {
    return {
      Circle,
      'Contract Account': ContractAccount,
      Division,
      Address,
      Meter,
      'CCMS Mtr No': CCMSMtrNo,
      'CCMS Panel No': CCMSPanelNo
    };
  }

  const rows = ccms_svr['SVR'];

  if (!rows) return null;
  return (
    <Table
      rows={rows}
      getTo={(row) =>
        '/asset_health/asset_details/Najafgar line Between Najafgarh and Nanglor 2- BTL 27/' +
        row['Division']
      }
      getKey={(row) => row['Asset ID']}
      labels={[
        'Circle',
        'Contract Account',
        'Division',
        'Address',
        'Meter',
        'CCMS Mtr No',
        'CCMS Panel No'
      ]}
    />
  );
};

export default CCMSList;
