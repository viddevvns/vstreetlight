/* eslint-disable react/prop-types */
import React from 'react';
import Typography from '@material-ui/core/Typography';
import Table from 'components/Table';

function createData(SL, description, Meters, Points, Load) {
  return {
    SL,
    'BRPL Division': description,
    Meters,
    'No of Points': Points,
    'Load (KW)': Load
  };
}

const rows = [
  createData(<b>1</b>, 'Sarita Vihar (SVR)', 268, 22988, 1287),
  createData(<b>2</b>, 'Khanpur (KHP)', 239, 21728, 1336),
  createData(<b>3</b>, 'Najafgarh (NJF)', 448, 35621, 2704),
  createData(<b>4</b>, 'Dwarka (DWK)', 280, 18267, 1268),
  createData(<b>5</b>, 'Palam (PLM)', 269, 25394, 1825),
  createData(<b>6</b>, 'Mohan Garden (MGN)', 123, 14009, 919),
  createData(<b>7</b>, 'Uttam Nagar (UTN)', 112, 7067, 464),
  createData(<b>8</b>, 'Vikaspuri (VKP)', 189, 14472, 897)
];

const CCMSDetails = ({ url }) => {
  return (
    <Table
      rows={rows}
      getTo={(row) =>
        '/ccms_details/'+row['SL']
      }
      getKey={(row) => row['SL']}
      labels={['SL', 'BRPL Division', 'Meters', 'No of Points', 'Load (KW)']}
    />
  );
};



export default CCMSDetails;
