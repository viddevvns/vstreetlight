/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUICard from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import MUICardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import { useTheme } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import MUIDialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import MUIDialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import MUITextField from '@material-ui/core/TextField';
import MUIPaper from '@material-ui/core/Paper';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Carousel as ReactCarousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import ZoomInRoundedIcon from '@material-ui/icons/ZoomInRounded';
import MUIIconButton from '@material-ui/core/IconButton';
import {
  getFaultList,
  getFaultImages,
  getFaultDetails
} from 'redux/actions/faults';
import {
  getMaintenanceInfo,
  getMaintenanceUsers,
  assignMaintenanceTask,
  updateTaskStatus
} from 'redux/actions/maintenance';
import { useDispatch, useSelector } from 'react-redux';
import Task from 'components/Task';
import CountDown from 'components/CountDown';
import getDateString from 'utils/getDateString';
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Typography = styled(MUITypography)`
  color: ${({ theme, $health }) => {
    if ($health === 'RED') return 'red';
    if ($health === 'YELLOW') return 'yellow';
    if ($health === 'AMBER') return '#ffbf00';
    if ($health === 'GREEN') return '#08bd80';
    return theme.colors.baseFont;
  }};
`;

const DialogTitle = styled(MUIDialogTitle)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const InfoButton = styled(MUIIconButton)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Card = styled(MUICard)`
  background-color: ${({ theme }) => theme.colors.paper};
  width: 360px;
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      width: 100%;
    }`};
  margin-top: ${({ $other }) => ($other && '40px') || 0};
`;

const View = styled(Button)`
  cursor: pointer;
  margin-bottom: 10px;
  background-color: #1de9b6;
    .MuiButton-label {
      color: ${({ theme }) => theme.colors.baseFontAlternate};
    }
  }
`;

const InfoContainer = styled.div`
  margin-top: 16px;
`;

const MaintenanceContainer = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const CardHeader = styled(MUICardHeader)`
  &.MuiCardHeader-root {
    .MuiTypography-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const Dialog = styled(MUIDialog)`
  &.MuiDialog-root .MuiDialog-container .MuiPaper-root {
    background: ${({ theme }) => theme.colors.paper};
    width: ${({ $carousel }) => ($carousel && '880px') || '550px'};
    max-width: ${({ $carousel }) => ($carousel && '880px') || '600px'};
    ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      width: 100%;
    }`};
  }
`;

const DatePicker = styled(KeyboardDatePicker)`
  &.MuiFormControl-root {
    margin: 0px;
    .MuiInput-underline:before {
      border-color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiInputBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
      .MuiInputAdornment-root {
        .MuiButtonBase-root {
          color: ${({ theme }) => theme.colors.baseFont};
        }
      }
    }
  }
`;

const TextField = styled(MUITextField)`
  &.MuiTextField-root {
    .MuiInput-underline:before {
      border-color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiInputBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
      .MuiAutocomplete-endAdornment {
        .MuiButtonBase-root {
          color: ${({ theme }) => theme.colors.baseFont};
        }
      }
    }
  }
`;

const Carousel = styled(ReactCarousel)`
  &.carousel-root {
    flex-grow: 1;
    display: flex;
    .carousel {
      /* height: 360px; */
      .slider-wrapper {
        /* height: 360px; */
        .slider {
          /* height: 360px; */
        }
      }
    }
  }
`;

const CarouselWrapper = styled(MUIPaper)`
  width: 600px;
  /* height: 400px; */
  padding: 20px;
  background-color: ${({ theme }) => theme.colors.paper};
  /* margin-left: 40px; */
  flex-grow: 1;
`;

const RowWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 40px;
  align-items: flex-start;
`;

const ImageWrapper = styled(MUIPaper)`
  height: 500px;
  position: relative;
  ${({ $dialog }) => {
    if ($dialog) return 'padding: 20px; height: 100%; width: 550px;';
  }}
`;

const Enlarge = styled(ZoomInRoundedIcon)`
  color: #fff;
`;

const IconButton = styled(MUIIconButton)`
  position: absolute;
  bottom: 5px;
  right: 25px;
  padding: 20px;
`;

const FaultDetails = ({ params, user }) => {
  const { towerId, faultId, lineId } = params;
  const [open, setOpen] = React.useState(false);
  const [carousel, setCarousel] = React.useState(false);
  const [assignedTo, setAssignedTo] = React.useState('');
  const [inputValue, setInputValue] = React.useState('');
  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const [selectedImg, setSelectedImg] = React.useState(0);
  const [showInfoImg, setShowInfoImg] = React.useState(false);

  const theme = useTheme();

  const handleToggleInfo = () => {
    setShowInfoImg(!showInfoImg);
  };

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleClickOpen = () => {
    setCarousel(false);
    setOpen(true);
  };

  const handleCarouselZoom = () => {
    setCarousel(true);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const faultIdTemp = '200901_F_T0008AR1_JUMP-LNB';
  const dispatch = useDispatch();
  let id = towerId;
  if (!towerId) id = faultId.slice(0, 5);
  useEffect(() => {
    dispatch(getFaultList(id));
    dispatch(getMaintenanceInfo(faultIdTemp));
    dispatch(getMaintenanceUsers());
    dispatch(getFaultImages(faultIdTemp));
    dispatch(getFaultDetails(faultIdTemp));
  }, [dispatch, towerId, faultId, id]);

  let faultImages =
    useSelector(({ faults }) => faults.images[faultIdTemp]) || [];
  let faultDetails = useSelector(({ faults }) => faults[faultIdTemp]);
  let details = useSelector(({ maintenance }) => maintenance[faultIdTemp]);
  const personals = useSelector(({ maintenance }) => maintenance.users);
  const onSliderChange = (index) => setSelectedImg(index);
  details = {
    'Maintenance ID': faultId,
    'Asset ID': towerId,
    'Task Status': 'Not Scheduled',
    'Assigned To': 'NA',
    'Assigned By': 'Admin',
    'Close Date': '2000-01-01T00:00:00Z',
    'Scheduled Date': '2000-01-01T00:00:00Z',
    'Operated Date': '2000-01-01T00:00:00Z',
    'Estimated Cost': '0',
    personnel_details: { Name: '-', 'Email ID': '-', 'Phone Number': '-' }
  };
  faultDetails = {
    'Fault Description': 'Precision Predictive Analysis',
    'Fault Type': 'Inspection',
    Health: 'AMBER',
    'Risk Level': 'Asset Health',
    Severity: '1',
    Status: 'Not Scheduled',
    reference_image:
      'https://s3-eu-west-1.amazonaws.com/vns.utils/FaultsPictionary/JUMP.JPG'
  };
  faultImages = ['https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/411_box.jpg', 'https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/411_edge.jpg', 'https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/411_opening.jpg', 'https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/416_box.jpg', 'https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/416_edge.jpg', 'https://s3-eu-west-1.amazonaws.com/vns.utils/vstreetlightutils/416_opening.jpg'];
  useEffect(() => {
    setAssignedTo(details?.personnel_details);
    const rawDate = details?.['Scheduled Date'];
    setSelectedDate(rawDate);
  }, [details]);

  const onAssign = async () => {
    handleClose();
    await dispatch(
      assignMaintenanceTask({
        data: {
          'Fault ID': faultId,
          'Assigned To': assignedTo.Name,
          'Assigned By': user.username,
          'Scheduled Date': selectedDate
        }
      })
    );
    window.location.reload();
  };

  if (!details) return null;

  const today = new Date();

  const dialogContent = () => {
    if (carousel) {
      console.log('carousel');
      return (
        <ImageWrapper $dialog>
          <img
            width="840px"
            src="https://img.saurenergy.com/2018/01/ezgif.com-resize.jpg"
            alt={selectedImg}
          />
        </ImageWrapper>
      );
    }
    return (
      <>
        <DialogTitle id="alert-dialog-slide-title">
          {`Assign Fault ID: ${faultId}`}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Maintenance Id
              </Typography>
              <Typography variant="body2" component="p">
                {details['Maintenance ID']}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Assigned to
              </Typography>
              {assignedTo && (
                <Autocomplete
                  id="assignee"
                  options={personals}
                  style={{ width: 220 }}
                  getOptionLabel={(option) => option.Name}
                  renderInput={(params) => <TextField {...params} />}
                  value={assignedTo}
                  onChange={(event, newValue) => {
                    setAssignedTo(newValue);
                  }}
                  inputValue={inputValue}
                  onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                  }}
                />
              )}
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Assigned date
              </Typography>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <DatePicker
                  minDate={today}
                  disableToolbar
                  variant="inline"
                  format="MM/dd/yyyy"
                  margin="normal"
                  id="date-picker-inline"
                  value={selectedDate}
                  onChange={handleDateChange}
                  KeyboardButtonProps={{
                    'aria-label': 'change date'
                  }}
                />
              </MuiPickersUtilsProvider>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Contact Number
              </Typography>
              <Typography variant="body2" component="p">
                {assignedTo?.['Phone Number']}
              </Typography>
            </MaintenanceContainer>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <View onClick={onAssign}>Submit</View>
        </DialogActions>
      </>
    );
  };

  const handleApprove = async () => {
    await dispatch(
      updateTaskStatus({
        data: {
          fault_id: faultId,
          update: { 'Task Status': 'Completed' }
        }
      })
    );
    window.location.reload();
  };

  const handleRejected = async () => {
    await dispatch(
      updateTaskStatus({
        data: {
          fault_id: faultId,
          update: { 'Task Status': 'Rejected' }
        }
      })
    );
    window.location.reload();
  };

  const getButton = () => {
    if (details?.['Task Status'] === 'Need Approval') {
      return (
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            width: '100%'
          }}
        >
          <View onClick={handleApprove} size="small">
            Approve
          </View>
          <View onClick={handleRejected} size="small">
            Reject
          </View>
        </div>
      );
    }
    if (details?.['Task Status'] === 'Not Scheduled') {
      return (
        <View onClick={handleClickOpen} size="small">
          Assign
        </View>
      );
    }
    if (
      details?.['Task Status'] === 'Rejected' ||
      details?.['Task Status'] === 'Completed'
    ) {
      return (
        <View onClick={handleClickOpen} size="small">
          Reassign
        </View>
      );
    }
  };
  return (
    <>
      <RowWrapper>
        {selectedDate && <CountDown date={selectedDate} />}
      </RowWrapper>

      <RowWrapper>
        {faultDetails && (
          <Card $theme={theme}>
            {/* <CardActionArea> */}
            <CardHeader
              action={
                <InfoButton aria-label="Info" onClick={handleToggleInfo}>
                  {showInfoImg ? <CloseRoundedIcon /> : <InfoOutlinedIcon />}
                </InfoButton>
              }
              title={`Fault Id : ${faultId}`}
              subheader={`Fault Type : ${faultDetails?.['Fault Type']}`}
            />
            {showInfoImg && (
              <CardMedia
                component="img"
                alt={faultDetails?.['Fault Type']}
                height="140"
                image={faultDetails?.reference_image}
                // title={currentProject.label}
              />
            )}
            {!showInfoImg && (
              <CardContent>
                {/* {currentProject.description && (
              <Typography variant="body2" component="p">
                {currentProject.description}
              </Typography>
            )} */}
                <InfoContainer>
                  <Typography
                    $health={faultDetails?.['Health']}
                    variant="h6"
                    component="p"
                  >
                    {faultDetails?.['Health']}
                  </Typography>
                  <Typography variant="body2" component="p">
                    Health
                  </Typography>
                </InfoContainer>
                <InfoContainer>
                  <Typography variant="h6" component="p">
                    {faultDetails?.['Status']}
                  </Typography>
                  <Typography variant="body2" component="p">
                    Status
                  </Typography>
                </InfoContainer>
                <InfoContainer>
                  <Typography variant="h6" component="p">
                    {faultDetails?.['Risk Level']}
                  </Typography>
                  <Typography variant="body2" component="p">
                    Risk Level
                  </Typography>
                </InfoContainer>
                <InfoContainer>
                  <Typography variant="h6" component="p">
                    {faultDetails?.['Severity']}
                  </Typography>
                  <Typography variant="body2" component="p">
                    Severity
                  </Typography>
                </InfoContainer>
                <InfoContainer>
                  <Typography variant="h6" component="p">
                    {faultDetails?.['Fault Description']}
                  </Typography>
                  <Typography variant="body2" component="p">
                    Fault Description
                  </Typography>
                </InfoContainer>
              </CardContent>
            )}
            {/* </CardActionArea> */}
            {/* <CardActions disableSpacing>
          <View
            size="small"
            color="primary"
            component={RouterLink}
            to={'/projects/current_project/200829_BRPL_NJF_NNG'}
          >
            View in detail
          </View>
          <Location component="p">
            <LocationOnRoundedIcon />
            {currentProject.location}
          </Location>
        </CardActions> */}
          </Card>
        )}
        <CarouselWrapper $theme={theme}>
          <Carousel
            showThumbs={false}
            onChange={(index) => onSliderChange(index)}
          >
            {faultImages?.map((image) => (
              <ImageWrapper key={image}>
                <img
                  src={image}
                  alt={image}
                  height="100%"
                  style={{ objectFit: 'cover' }}
                />
                <IconButton onClick={handleCarouselZoom}>
                  {/* <div style={{ width: '100%', height: '100%' }}> */}
                  <Enlarge />
                  {/* </div> */}
                </IconButton>
              </ImageWrapper>
            ))}
          </Carousel>
        </CarouselWrapper>
      </RowWrapper>
      {/* {user.access_role !== 'level_3' && ( */}
      <Card $other={true} $theme={theme}>
        <CardActionArea>
          <CardHeader
            avatar={
              <Avatar aria-label="recipe">
                {details?.['Assigned To']?.slice(0, 1)}
              </Avatar>
            }
            title={`Maintenance Id : ${details?.['Maintenance ID']}`}
            subheader={`Assigned To : ${assignedTo?.Name}`}
          />
          {/* {currentProject.image && (
            <CardMedia
              component="img"
              alt={currentProject.label}
              height="140"
              image={currentProject.image}
              title={currentProject.label}
            />
          )} */}
          <CardContent>
            {/* {currentProject.description && (
              <Typography variant="body2" component="p">
                {currentProject.description}
              </Typography>
            )} */}
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Task Status
              </Typography>
              <Typography variant="body2" component="p">
                {details?.['Task Status']}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Estimated Cost
              </Typography>
              <Typography variant="body2" component="p">
                {details?.['Estimated Cost']}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Close Date
              </Typography>
              <Typography variant="body2" component="p">
                {getDateString(details?.['Close Date'])}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Scheduled Date
              </Typography>
              <Typography variant="body2" component="p">
                {getDateString(selectedDate)}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Operated Date
              </Typography>
              <Typography variant="body2" component="p">
                {getDateString(details?.['Operated Date'])}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Email ID
              </Typography>
              <Typography variant="body2" component="p">
                {assignedTo?.['Email ID']}
              </Typography>
            </MaintenanceContainer>
            <MaintenanceContainer>
              <Typography variant="body2" component="p">
                Contact Number
              </Typography>
              <Typography variant="body2" component="p">
                {assignedTo?.['Phone Number']}
              </Typography>
            </MaintenanceContainer>
          </CardContent>
        </CardActionArea>
        {user.access_role !== 'level_3' && (
          <CardActions disableSpacing>
            {getButton()}
            {/* <Location component="p">
            <LocationOnRoundedIcon />
            {currentProject.location}
          </Location> */}
          </CardActions>
        )}
      </Card>
      {/* )} */}
      <div style={{ flexGrow: 1, marginTop: 40 }}>
        <Task
          disabled={user.access_role !== 'level_3'}
          faultId={faultId}
          user={user}
        />
      </div>

      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
        $theme={theme}
        $carousel={carousel}
      >
        {dialogContent()}
      </Dialog>
    </>
  );
};

export default FaultDetails;
