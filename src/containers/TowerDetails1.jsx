/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Typography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import Table from 'components/Table';
import { getTowerInfo } from 'redux/actions/towers';
import { getFaultList } from 'redux/actions/faults';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-bottom: 40px;
  display: flex;
  flex-wrap: wrap;
`;

const Label = styled(Typography)`
  color: ${({ theme, $health }) => {
    if ($health === 'RED') return '#c62828';
    if ($health === 'YELLOW') return '#ffeb3b';
    if ($health === 'AMBER') return '#ffa000';
    if ($health === 'GREEN') return 'green';
    return theme.colors.baseFont;
  }};
`;

const InfoContainer = styled.div`
  margin-top: 16px;
  flex: 1 1 50%;
`;

const Header = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  margin-top: 40px;
  margin-bottom: 30px;
`;
const TowerDetails = ({ params }) => {
  const { towerId, lineId } = params;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getTowerInfo(towerId));
    dispatch(getFaultList(towerId));
  }, [dispatch, towerId]);
  const details = useSelector(({ towers }) => towers[towerId]);
  function createData(ID, Date, Time, Activity, Responsible) {
    return {
      'Activity ID': ID + towerId,
      'Asset ID': towerId,
      Date,
      Time,
      Activity,
      Responsible
    };
  }
  function createMaintenanceData(ID, Type, Health, Risk, Severity, Status) {
    return {
      'Maintenance ID': ID + towerId,
      Type,
      Health,
      'Risk Level': Risk,
      Severity,
      Status
    };
  }
  const rows_maintenance = [
    createMaintenanceData(
      '2021-02-16_',
      'Inpection',
      'Amber',
      'Asset Health',
      '2',
      'Not Scheduled'
    ),
    createMaintenanceData(
      '2021-03-16_',
      'Inpection',
      'Green',
      'Asset Health',
      '1',
      'Not Scheduled'
    ),
    createMaintenanceData(
      '2021-04-16_',
      'Inpection',
      'Green',
      'Asset Health',
      '1',
      'Not Scheduled'
    ),
    createMaintenanceData(
      '2021-05-16_',
      'Inpection',
      'Green',
      'Asset Health',
      '1',
      'Not Scheduled'
    )
  ];
  const rows_activites = [
    createData('2021-01-16T05:30_', '16/01/21', '5:32', 'Turned Off', 'Admin'),
    createData('2021-01-16T05:30_', '16/01/21', '18:23', 'Turned On', 'Admin'),
    createData('2021-01-16T05:30_', '17/01/21', '5:23', 'Turned Off', 'Admin'),
    createData('2021-01-16T05:30_', '17/01/21', '18:44', 'Turned On', 'Admin')
  ];
  if (!details) {
    return null;
  } else {
    console.log('details');
    console.log(details);
    Object.keys(details).map((detail) => console.log(detail));
  }
  return (
    <>
      <Paper>
        {Object.keys(details).map((detail) => (
          <InfoContainer key={detail}>
            <Label $health={details[detail]} variant="h6" component="p">
              {details[detail]}
            </Label>
            <Label variant="body2" component="p">
              {detail}
            </Label>
          </InfoContainer>
        ))}
      </Paper>
      <Header>
        <Label variant="h6">List of Maintenance</Label>
      </Header>
      <Table
        rows={rows_maintenance}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Maintenance ID']}`
        }
        getKey={(row) => row['Maintenance ID']}
        labels={[
          'Maintenance ID',
          'Type',
          'Health',
          'Risk Level',
          'Severity',
          'Status'
        ]}
      />
      <Header>
        <Label variant="h6">List of Activities</Label>
      </Header>
      <Table
        rows={rows_activites}
        getTo={(row) =>
          `/asset_health/asset_details/${lineId}/${towerId}/${row['Asset ID']}`
        }
        getKey={(row) => row['Activity ID']}
        labels={[
          'Activity ID',
          'Asset ID',
          'Date',
          'Time',
          'Activity',
          'Responsible'
        ]}
      />
    </>
  );
};

export default TowerDetails;
