/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Frame = styled.iframe`
  height: calc(100% - 170px);
`;

const VegetativeHealth = () => {
  return (
    <>
      <Typography paragraph>BRPL_vegetation_Management</Typography>
      <Frame
        src="https://www.google.com/maps/d/u/0/embed?mid=1pd1I6vdo4qnCMIHqozhxYrdE8k_iDRYM"
        width="100%"
        title="Vegetative Health"
        frameBorder="0"
      />
    </>
  );
};

export default VegetativeHealth;
