/* eslint-disable eqeqeq */
/* eslint-disable react/prop-types */
/* eslint-disable import/no-unresolved */
import React, { useState } from 'react';
import Layout from 'layout';
import { StylesProvider } from '@material-ui/core/styles';
import styled, { ThemeProvider } from 'styled-components';
import CssBaseline from '@material-ui/core/CssBaseline';
import ReduxLoadingBar from 'react-redux-loading-bar';
import AppBar from 'components/AppBar';

import Routes from 'routes';
import { lightTheme, darkTheme } from 'themes';

function setCookie(name, value, days) {
  var expires = '';
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = '; expires=' + date.toUTCString();
  }
  document.cookie = name + '=' + (value || '') + expires + '; path=/';
}
function getCookie(name) {
  var nameEQ = name + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}

const LoadingBar = styled(ReduxLoadingBar)`
  height: 4px;
  background-color: #1de9b6;
  position: fixed;
  top: 0;
  z-index: 1202;
`;

const BackgroundImage = 'https://images.pexels.com/photos/4916474/pexels-photo-4916474.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260';

const Root = styled.div`
  width: 100%;
  height: 100vh;
  overflow: hidden;
  background-color: ${({ theme }) => theme.colors.base};
  display: flex;
`;

const Test = ({ children }) => (
  <div style={{ margin: 'auto' }}>
    <AppBar
      position="fixed"
      handleDrawerToggle={() => {}}
      drawerWidth={260}
      layoutTheme={'light'}
      handleLight={() => {}}
      handleDark={() => {}}
    />
    {children}
  </div>
);

const App = () => {
  let browserTheme = 'light';
  if (
    window.matchMedia &&
    window.matchMedia('(prefers-color-scheme: dark)').matches
  ) {
    browserTheme = 'dark';
  }
  const [theme, setTheme] = useState(browserTheme);
  const user1 = getCookie('user');
  const [user, setUser] = useState(JSON.parse(user1));

  const checkStatus = (response) => {
    if (response.ok) return response;
    const error = new Error(response.statusText);
    error.response = response;
    return Promise.reject(error);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    const email = e.target.email.value;
    const password = e.target.password.value;
    fetch('https://vnautilus.vidrona.com/vUserAuthentication/login', {
      method: 'post',
      body: JSON.stringify({
        email_id: email,
        password
      })
    })
      .then(checkStatus)
      .then((response) => response.json())
      .then((user) => {
        setUser(user);
        setCookie('user', JSON.stringify(user));
      })
      .catch(console.log);
  };

  const handleLogout = (e) => {
    e.preventDefault();
    setUser(false);
  };

  const handleLight = () => {
    setTheme('light');
  };

  const handleDark = () => {
    setTheme('dark');
  };

  let VLayout = Test;
  if (user) VLayout = Layout;

  return (
    <ThemeProvider theme={theme === 'light' ? lightTheme : darkTheme}>
      <LoadingBar updateTime={100} />
      <Root>
        <StylesProvider injectFirst>
          <CssBaseline />
          <VLayout
            layoutTheme={theme}
            handleLight={handleLight}
            handleDark={handleDark}
            user={user}
          >
            <Routes
              handleLogout={handleLogout}
              handleLogin={handleLogin}
              user={user}
            />
          </VLayout>
        </StylesProvider>
      </Root>
    </ThemeProvider>
  );
};

export default App;
