/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import MUITypography from '@material-ui/core/Typography';
import MUITextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { postTaskChat, getTaskChat } from 'redux/actions/maintenance';
import getDateString from 'utils/getDateString';

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const TextField = styled(MUITextField)`
  &.MuiTextField-root {
    width: 100%;
    .MuiInput-underline:before {
      border-color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiInputBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
      .MuiOutlinedInput-input {
        padding: 10px 14px;
      }
      .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.colors.border};
      }
      .MuiAutocomplete-endAdornment {
        .MuiButtonBase-root {
          color: ${({ theme }) => theme.colors.baseFont};
        }
      }
    }
  }
`;

const Triangle = styled.div`
  width: 0;
  height: 0;
  border-bottom: 10px solid transparent;
  border-right: 10px solid ${({ theme }) => theme.colors.chat};
`;

const Text = styled.div`
  padding: 5px 10px;
  background-color: ${({ theme }) => theme.colors.chat};
  border-radius: 5px;
  border-top-left-radius: 0;
  margin-bottom: 5px;
`;

const Author = styled(Typography)`
  font-size: 12px;
`;

const TextContainer = styled.div`
  display: flex;
`;

const ChatText = ({ message, author, timestamp }) => (
  <TextContainer>
    <Triangle />
    <Text>
      <Typography>{message}</Typography>
      <div>
        <Author>
          {author} &middot; {getDateString(timestamp)}
        </Author>
      </div>
    </Text>
  </TextContainer>
);

const FieldContainer = styled.div`
  display: flex;
  width: 100%;
  position: sticky;
  bottom: 0px;
  padding-bottom: 20px;
  background-color: ${({ theme }) => theme.colors.paper};
  gap: 20px;
`;

const Container = styled.div`
  border: 1px solid ${({ theme }) => theme.colors.border};
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  height: 300px;
  overflow: scroll;
  position: relative;
  padding-bottom: 0px;
`;

const Chat = ({ faultId, user }) => {
  const [message, setMessage] = React.useState('');

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTaskChat(faultId));
  }, [dispatch, faultId]);

  const handleSend = () => {
    if (message)
      dispatch(
        postTaskChat(
          {
            data: {
              fault_id: faultId,
              update: { author: user.username, message }
            }
          },
          faultId
        )
      );
    setMessage('');
  };

  const chats = useSelector(
    ({ maintenance }) => maintenance?.chats?.[faultId]?.['Task Chat'] || []
  );

  return (
    <Container>
      {chats.map((chat, index) => (
        <ChatText key={index} {...chat} />
      ))}
      <FieldContainer>
        <TextField
          placeholder="Description about the task"
          variant="outlined"
          value={message}
          onChange={(e) => setMessage(e.target.value)}
        />
        <Button variant="contained" color="primary" onClick={handleSend}>
          Send
        </Button>
      </FieldContainer>
    </Container>
  );
};

export default Chat;
