/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import MUIPaper from '@material-ui/core/Paper';
import MUITypography from '@material-ui/core/Typography';
import styled from 'styled-components';
import useCountDown from 'hooks/useCountDown';

const Paper = styled(MUIPaper)`
  padding: 20px;
  background: ${({ theme }) => theme.colors.paper};
  width: 360px;
  margin-bottom: 40px;
  text-align: center;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme, $remaining }) => {
    if ($remaining < 7) return 'red';
    return theme.colors.baseFont;
  }};
`;

const CountDown = ({ date }) => {
  const [timeLeft, setTimeLeft] = useState(0);
  useCountDown(setTimeLeft, date);
  return (
    <Paper>
      <Typography variant="h6" component="p">
        Days remaining
      </Typography>
      <Typography $remaining={timeLeft} variant="h4" component="p">
        {timeLeft}
      </Typography>
    </Paper>
  );
};

export default CountDown;
