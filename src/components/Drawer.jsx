/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUIDrawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import MUIDivider from '@material-ui/core/Divider';
import { ReactComponent as ProjectsIcon } from 'images/drawer/projects.svg';
import { ReactComponent as AssetHealthIcon } from 'images/drawer/asset_health.svg';
import { ReactComponent as AnalyticsIcon } from 'images/drawer/analytics.svg';
import { ReactComponent as MaintenanceWorkIcon } from 'images/drawer/maintenance_work.svg';
import { ReactComponent as WeatherIcon } from 'images/drawer/weather.svg';
import { ReactComponent as ResilienceIcon } from 'images/drawer/resilience.svg';
import { ReactComponent as VegetativeHealthIcon } from 'images/drawer/vegetative_health.svg';
import { ReactComponent as CompletedProjectIcon } from 'images/drawer/completed_project.svg';
import { ReactComponent as Conductor } from 'images/drawer/conductor.svg';
import Assignment from '@material-ui/icons/AssignmentRounded';
import Poll from '@material-ui/icons/PollRounded';
import StarBorder from '@material-ui/icons/StarBorder';
import { useTheme } from '@material-ui/core/styles';
import RenderListItem from 'components/ListItem';
import ShowChartRoundedIcon from '@material-ui/icons/ShowChartRounded';

const upperIconSet = [
  {
    name: 'Projects',
    Component: ProjectsIcon,
    childs: [
      {
        name: 'Current Projects',
        Component: Assignment,
        nav: '/projects/current_project'
      },
      {
        name: 'Completed Projects',
        Component: CompletedProjectIcon,
        nav: '/projects/completed_project',
        disabled: true
      },
      {
        name: 'All Projects',
        Component: StarBorder,
        nav: '/projects/all_projects',
        disabled: true
      }
    ]
  },
  {
    name: 'Asset Health',
    Component: AssetHealthIcon,
    childs: [
      {
        name: 'Asset Details',
        Component: ShowChartRoundedIcon,
        nav: '/asset_health/asset_details'
      },
      {
        name: 'Asset Info',
        Component: ShowChartRoundedIcon,
        nav: '/asset_health/asset_info'
      },
      {
        name: 'CCMS Details',
        Component: ShowChartRoundedIcon,
        nav: '/asset_health/ccms_details'
      }
    ]
  },
  {
    name: 'Vegetative Health',
    Component: VegetativeHealthIcon,
    nav: '/vegetative_health'
  },
  {
    name: 'Street Lights Maps',
    Component: VegetativeHealthIcon,
    nav: '/streetlightmaps'
  },
  {
    name: 'Satellite Monitoring Grid-System',
    Component: VegetativeHealthIcon,
    nav: '/StreetLightsSatellite'
  },
  {
    name: 'Satellite View',
    Component: StarBorder,
    nav: '/satellite-view'
  },
  {
    name: 'CCMS',
    Component: Conductor,
    childs: [
      {
        name: 'CCMS Details',
        Component: Conductor,
        nav: '/ccms_details'
      },
      {
        name: 'CCMS Analytics',
        Component: Conductor,
        nav: '/ccms_analytics'
      }
    ]
  },
  {
    name: 'Reliability',
    Component: StarBorder,
    nav: '/reliability'
  },
  {
    name: 'Analytics',
    Component: AnalyticsIcon,
    childs: [
      {
        name: 'Maintenance Analysis',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      },
      {
        name: 'Assets Performance',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/assets_performance'
      },
      {
        name: 'Line Availability',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/line_availability'
      },
      {
        name: 'Asset Kundali',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      },
      {
        name: 'Region Analytics',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      },
      {
        name: 'Reliability Analysis',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      },
      {
        name: 'O&M Performance',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      },
      {
        name: 'Earth Observation Ananlysis',
        Component: ShowChartRoundedIcon,
        nav: '/analysis/maintenance_analysis'
      }
    ]
  }
];

const lowerIconSet = [
  {
    name: 'My Task',
    Component: Assignment,
    nav: '/my_task'
  },
  {
    name: 'Maintenance Work',
    Component: MaintenanceWorkIcon,
    childs: [
      {
        name: 'Line Maintenance',
        Component: MaintenanceWorkIcon,
        nav: '/'
      },
      {
        name: 'Track Maintenance',
        Component: MaintenanceWorkIcon,
        nav: '/'
      }
    ]
  },
  {
    name: 'Survey',
    Component: Poll,
    nav: '/survey'
  },
  {
    name: 'Resilience',
    Component: ResilienceIcon,
    nav: 'projects',
    disabled: true
  },
  {
    name: 'Weather',
    Component: WeatherIcon,
    nav: 'projects',
    disabled: true
  }
];

const Drawer = styled(MUIDrawer)`
  &.MuiDrawer-root {
    .MuiPaper-root {
      background-color: ${({ theme }) => theme.colors.paper};
      width: ${({ $drawerWidth }) => `${$drawerWidth}px`};
      box-shadow: 4px 0 5px -2px ${({ theme }) => theme.colors.border};
      border-color: ${({ theme }) => theme.colors.border};
      ${({ $theme }) => `${[$theme.breakpoints.up('sm')]} {
      padding-top: 64px;
    }`};
    }
  }
`;

const Divider = styled(MUIDivider)`
  background-color: ${({ theme }) => theme.colors.border};
`;

const NavBar = ({
  open,
  onClose,
  drawerWidth,
  container,
  variant,
  anchor,
  ModalProps
}) => {
  const theme = useTheme();

  return (
    <Drawer
      variant={variant}
      $drawerWidth={drawerWidth}
      container={container}
      open={open}
      onClose={onClose}
      anchor={anchor}
      ModalProps={ModalProps}
      $theme={theme}
    >
      <List>
        {upperIconSet.map((icon, index) => (
          <RenderListItem key={icon.name} icon={icon} onClose={onClose} />
        ))}
      </List>
      <Divider />
      <List>
        {lowerIconSet.map((icon, index) => (
          <RenderListItem key={icon.name} icon={icon} onClose={onClose} />
        ))}
      </List>
    </Drawer>
  );
};

export default NavBar;
