import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import styled from 'styled-components';
import React from 'react';

const StyledPaper = styled(Paper)`
  padding: 16px;
  min-height: 64px;
  background-color: ${({ theme }) => theme.colors.paper};
`;

const H1 = styled.h1`
  color: ${({ theme }) => theme.colors.baseFont};
`;

export default function Card({ title, children, className }) {
  return (
    <StyledPaper className={className}>
      {title ? <H1>{title}</H1> : null}
      {children}
    </StyledPaper>
  );
}

Card.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.any.isRequired,
  className: PropTypes.string
};

Card.defaultProps = {
  title: '',
  className: ''
};
