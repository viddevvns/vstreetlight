/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from 'components/Stepper';
import Preparedness from 'components/Preparedness';
import Button from '@material-ui/core/Button';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import FormGroup from '@material-ui/core/FormGroup';
import MUIFormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MUITextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import {
  getMaintenanceTaskProgress,
  getStepperLabels,
  updatePreparedness,
  updateTaskOperations,
  updateTaskStatus
} from 'redux/actions/maintenance';
import { useDispatch, useSelector } from 'react-redux';
import Chat from './Chat';
import Uploader from './Uploader';

const Root = styled.div`
  width: 100%;
`;

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  width: 100%;
`;

const FormControlLabel = styled(MUIFormControlLabel)`
  &.MuiFormControlLabel-root {
    .MuiButtonBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiTypography-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const MaintenanceContainer = styled.div`
  display: flex;
  gap: 40px;
  align-items: center;
  justify-content: space-between;
  width: 700px;
  margin-bottom: 20px;
`;

const Typography = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const TextField = styled(MUITextField)`
  &.MuiTextField-root {
    width: 300px;
    .MuiInput-underline:before {
      border-color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiInputBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
      .MuiOutlinedInput-notchedOutline {
        border-color: ${({ theme }) => theme.colors.border};
      }
      .MuiAutocomplete-endAdornment {
        .MuiButtonBase-root {
          color: ${({ theme }) => theme.colors.baseFont};
        }
      }
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  button: {
    marginRight: theme.spacing(1)
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  }
}));

// const options = [
//   { title: 'Repair' },
//   { title: 'Cleaning' },
//   { title: 'Mechanical Type (tightening)' },
//   { title: 'Component replacement' },
//   { title: 'Normal inspection' }
// ];

const Report = ({
  step,
  images,
  handleImage,
  disabled,
  confirm,
  setConfirm,
  faultId,
  user
}) => {
  if (step !== 2) return null;
  return (
    <Paper>
      <Typography variant="body1">Remarks</Typography>
      <Chat faultId={faultId} user={user} />
      <MaintenanceContainer>
        <Typography variant="body1" component="label">
          Uploaded Images
        </Typography>
        <Uploader faultId={faultId} user={user} />
      </MaintenanceContainer>
      <MaintenanceContainer>
        <FormGroup>
          <FormControlLabel
            control={
              <Checkbox checked={confirm} onChange={setConfirm} name={0} />
            }
          />
        </FormGroup>
        <Typography variant="body1" component="label">
          I have made the necessary maintenance work
        </Typography>
      </MaintenanceContainer>
    </Paper>
  );
};

const Maintenance = ({
  step,
  maintenance,
  setMaintenance,
  costings,
  setCostings,
  componentReplaced,
  setcomponentReplaced,
  inputValueComponent,
  setInputValueComponent,
  disabled,
  labels
}) => {
  if (step !== 1 || !labels) return null;
  return (
    <Paper>
      <MaintenanceContainer>
        <Typography variant="body1" component="label">
          Maintenance Activity Type
        </Typography>
        <Typography variant="body1" component="label">
          {labels['Maintenance Activity Type']}
        </Typography>
      </MaintenanceContainer>
      <MaintenanceContainer>
        <Typography variant="body1" component="label">
          No of Component replaced
        </Typography>
        <TextField
          style={{ width: 300 }}
          value={componentReplaced}
          onChange={(e) => setcomponentReplaced(e.target.value)}
          type="number"
          disabled={disabled}
        />
      </MaintenanceContainer>
      <MaintenanceContainer>
        <Typography variant="body1" component="label">
          Costings
        </Typography>
        <TextField
          style={{ width: 300 }}
          value={costings}
          onChange={(e) => setCostings(e.target.value)}
          type="number"
          disabled={disabled}
        />
      </MaintenanceContainer>
    </Paper>
  );
};

export default function Task({ disabled, faultId, user }) {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const [preparedness, setPreparedness] = React.useState({
    0: false,
    1: false,
    2: false,
    3: false
  });
  const [images, setImages] = React.useState({
    0: false,
    1: false
  });
  const [maintenance, setMaintenance] = React.useState(null);
  const [componentReplaced, setcomponentReplaced] = React.useState('');
  const [inputValueComponent, setInputValueComponent] = React.useState('');
  const [costings, setCostings] = React.useState('');
  const [completedSteps, setCompletedSteps] = useState({});
  const [confirm, setConfirm] = useState(false);

  const dispatch = useDispatch();
  const handleConfirm = () => setConfirm(!confirm);

  useEffect(() => {
    dispatch(getMaintenanceTaskProgress(faultId));
    dispatch(getStepperLabels(faultId));
  }, [dispatch, faultId]);

  const stepLabels = useSelector(
    ({ maintenance }) =>
      maintenance?.labels?.[faultId]?.['Task Progress Stage']?.details
  );

  useEffect(() => {
    setCostings(stepLabels?.['During Maintenance']?.Costings);
    setcomponentReplaced(
      stepLabels?.['During Maintenance']?.['No of Component replaced']
    );
  }, [stepLabels]);

  const maintenanceStatus = useSelector(
    ({ maintenance }) =>
      maintenance[faultId]?.status?.['Task Progress Stage']?.status
  );

  useEffect(() => {
    const completed = {};
    if (maintenanceStatus)
      Object.values(maintenanceStatus).forEach((value, index) => {
        completed[index] = Boolean(value);
      });
    setCompletedSteps(completed);
    if (maintenanceStatus?.Preparedness) {
      setPreparedness({ 0: true, 1: true, 2: true, 3: true });
    }
  }, [maintenanceStatus]);

  const handleChange = (event) => {
    if (disabled) return;
    setPreparedness({
      ...preparedness,
      [event.target.name]: event.target.checked
    });
  };

  const handleImage = (event) => {
    setImages({
      ...images,
      [event.target.name]: event.target.checked
    });
  };

  const steps = ['Preparedness', 'During Maintenance', 'Reporting Maintenance'];

  const handleNext = () => {
    const prepared = Object.values(preparedness).reduce(
      (sum, next) => sum && next,
      true
    );

    if (activeStep === 0) {
      if (user.access_role === 'level_3' && prepared) {
        dispatch(
          updatePreparedness({
            data: {
              fault_id: faultId,
              update: { Preparedness: 1 }
            }
          })
        );
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
      } else if (user.access_role !== 'level_3')
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
    if (activeStep === 1) {
      if (
        componentReplaced !== '' &&
        costings !== '' &&
        user.access_role === 'level_3'
      ) {
        dispatch(
          updateTaskOperations({
            data: {
              fault_id: faultId,
              update: {
                'No of Component replaced': componentReplaced,
                'Performed By': user.username,
                Costings: costings
              }
            }
          })
        );
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
      } else if (user.access_role !== 'level_3')
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
    if (activeStep === 2) {
      if (confirm && user.access_role === 'level_3') {
        dispatch(
          updateTaskStatus({
            data: {
              fault_id: faultId,
              update: { 'Task Status': 'Need Approval' }
            }
          })
        );
        setActiveStep((prevActiveStep) => 0);
      } else if (user.access_role !== 'level_3')
        setActiveStep((prevActiveStep) => 0);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleTabChange = (index) => () => setActiveStep(index);

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleCostings = (value) => {
    if (value > -1) setCostings(value);
  };

  const handleComponentReplaced = (value) => {
    if (value > -1) setcomponentReplaced(value);
  };

  const button = () => {
    if (activeStep === steps.length - 1 && user.access_role !== 'level_3')
      return;
    return (
      <Button
        variant="contained"
        color="primary"
        onClick={handleNext}
        className={classes.button}
      >
        {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
      </Button>
    );
  };

  return (
    <Root>
      <Stepper
        activeStep={activeStep}
        steps={steps}
        nonLinear={disabled}
        handleStep={handleTabChange}
        completed={completedSteps}
      />

      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>
              All steps completed - you&apos;re finished
            </Typography>
            <Button onClick={handleReset} className={classes.button}>
              Reset
            </Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>
              <Preparedness
                step={activeStep}
                preparedness={preparedness}
                handleChange={handleChange}
                labels={stepLabels?.Preparedness}
                disabled={disabled}
              />
              <Maintenance
                step={activeStep}
                maintenance={maintenance}
                setMaintenance={setMaintenance}
                costings={costings}
                setCostings={handleCostings}
                componentReplaced={componentReplaced}
                setcomponentReplaced={handleComponentReplaced}
                inputValueComponent={inputValueComponent}
                setInputValueComponent={setInputValueComponent}
                disabled={disabled}
                labels={stepLabels?.['During Maintenance']}
              />
              <Report
                step={activeStep}
                images={images}
                handleImage={handleImage}
                confirm={confirm}
                setConfirm={handleConfirm}
                faultId={faultId}
                user={user}
              />
            </Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={handleBack}
                className={classes.button}
              >
                Back
              </Button>
              {button()}
            </div>
          </div>
        )}
      </div>
    </Root>
  );
}
