import React from 'react';
import './Unauthorized.css';

const Unauthorized = () => {
  return (
    <>
      <div id="app">
        <div>403</div>
        <div className="txt">
          Forbidden<span className="blink">_</span>
        </div>
      </div>
    </>
  );
};

export default Unauthorized;
