/* eslint-disable react/prop-types */

import React from 'react';
import styled from 'styled-components';
import List from '@material-ui/core/List';
import MUIListItem from '@material-ui/core/ListItem';
import MUIListItemText from '@material-ui/core/ListItemText';
import MUIListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';
import { Link } from 'react-router-dom';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

const ListItemOpener = styled(MUIListItemIcon)`
  color: ${({ theme }) => theme.colors.baseFont};
  min-width: initial;
`;

const Icon = styled.div`
  width: 25px;
  fill: ${({ theme }) => theme.colors.baseFont};
`;

const ListItemText = styled(MUIListItemText)`
  color: ${({ theme }) => theme.colors.baseFont};
  &.MuiListItemText-root {
    .MuiListItemText-primary {
      font-size: 0.85rem;
    }
  }
`;

const ListItem = styled(MUIListItem)`
  ${({ $nested }) => $nested && 'padding-left: 32px'};
`;

const ListItemIcon = styled(MUIListItemIcon)`
  color: ${({ theme }) => theme.colors.baseFont};
  width: 25px;
  height: 25px;
`;

const RenderChildNavs = ({ child, open, onClose }) => {
  const { name, Component, nav, disabled } = child;
  let element = (
    <ListItem disabled={disabled} $nested button>
      <ListItemIcon>
        <Icon>
          <Component />
        </Icon>
      </ListItemIcon>
      <ListItemText primary={name} />
    </ListItem>
  );
  if (disabled) return element;
  return (
    <Link to={nav} onClick={onClose}>
      {element}
    </Link>
  );
};

const RenderListItem = ({ icon, onClose }) => {
  const { name, Component, nav, childs, disabled } = icon;
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    if (!childs) return;
    setOpen(!open);
  };

  let element = (
    <ListItem disabled={disabled} button onClick={handleClick}>
      <ListItemIcon>
        <Icon>
          <Component />
        </Icon>
      </ListItemIcon>
      <ListItemText primary={name} />
      {childs && (
        <ListItemOpener>
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemOpener>
      )}
    </ListItem>
  );
  if (nav && !disabled)
    element = (
      <Link to={nav} onClick={onClose}>
        {element}
      </Link>
    );
  if (childs)
    element = (
      <>
        {element}
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List disablePadding>
            {childs.map((child, index) => (
              <RenderChildNavs
                key={child.name}
                child={child}
                open={open}
                onClose={onClose}
              />
            ))}
          </List>
        </Collapse>
      </>
    );
  return element;
};

export default RenderListItem;
