/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUIPaper from '@material-ui/core/Paper';
import FormGroup from '@material-ui/core/FormGroup';
import MUIFormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 20px;
  width: 100%;
`;

const FormControlLabel = styled(MUIFormControlLabel)`
  &.MuiFormControlLabel-root {
    .MuiButtonBase-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
    .MuiTypography-root {
      color: ${({ theme }) => theme.colors.baseFont};
    }
  }
`;

const Preparedness = ({
  step,
  preparedness,
  handleChange,
  labels,
  disabled
}) => {
  if (step !== 0 || !labels) return null;
  return (
    <Paper>
      <FormGroup>
        {labels.map((label, index) => (
          <FormControlLabel
            key={label}
            control={
              <Checkbox
                checked={preparedness[index]}
                onChange={handleChange}
                name={index}
                disabled={disabled}
              />
            }
            label={label}
          />
        ))}
      </FormGroup>
    </Paper>
  );
};

export default Preparedness;
