/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';
import MUILink from '@material-ui/core/Link';
import MUIBreadcrumbs from '@material-ui/core/Breadcrumbs';

const Link = styled(MUILink)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Breadcrumbs = styled(MUIBreadcrumbs)`
  margin-bottom: 40px;
  &.MuiBreadcrumbs-root {
    .MuiBreadcrumbs-ol {
      .MuiBreadcrumbs-separator {
        color: ${({ theme }) => theme.colors.baseFont};
      }
    }
  }
`;

const getLabel = (label, params) => (params[label] ? params[label] : label);
const getPath = (path, params) => {
  Object.keys(params).forEach((key) => {
    path = path.split(`:${key}`).join(params[key]);
  });
  return path;
};

const BreadCrumb = ({ breadCrumb, params }) => (
  <Breadcrumbs aria-label="breadcrumb">
    {breadCrumb &&
      breadCrumb.map(({ label, path }) => (
        <Link key={label} component={RouterLink} to={getPath(path, params)}>
          {getLabel(label, params)}
        </Link>
      ))}
  </Breadcrumbs>
);

export default BreadCrumb;
