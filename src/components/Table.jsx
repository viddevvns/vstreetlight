/* eslint-disable react/prop-types */

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MUITable from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';
import Collapse from '@material-ui/core/Collapse';
import MUIIconButton from '@material-ui/core/IconButton';
import MUIPaper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useStyles = makeStyles({
  table: {
    minWidth: 700
  }
});

const StyledTableCell = styled(TableCell)`
  color: ${({ theme, $health }) => {
    const health = $health?.toLowerCase && $health.toLowerCase();
    if (health === 'red') return 'red';
    if (health === ' red') return 'red';
    if (health === 'yellow') return 'yellow';
    if (health === 'amber') return '#ffbf00';
    if (health === 'green') return '#08bd80';
    return theme.colors.baseFont;
  }};
  border-color: ${({ theme }) => theme.colors.tableBorder};
  cursor: pointer;
`;

const IconButton = styled(MUIIconButton)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  padding: 40px;
  width: 100%;
`;

const StyledTableRow = styled(TableRow)`
  cursor: pointer;
  background-color: ${({ theme }) => theme.colors.table};
  :nth-of-type(odd) {
    background-color: ${({ theme }) => theme.colors.tableAlternate};
  }
`;

const Head = styled(TableRow)`
  background-color: ${({ theme }) => theme.colors.table};
`;

const Rows = ({
  rows,
  getTo,
  getKey,
  labels,
  expansion,
  row,
  index,
  colSpan,
  onExpansion,
  ExpansionData
}) => {
  const [open, setOpen] = React.useState(false);

  const onExpand = () => {
    setOpen(!open);
    onExpansion && onExpansion(row);
  };

  return (
    <>
      <StyledTableRow key={getKey(row, index)}>
        {expansion && (
          <StyledTableCell>
            <IconButton aria-label="expand row" size="small" onClick={onExpand}>
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </StyledTableCell>
        )}
        {labels.map((label) => (
          <StyledTableCell
            $health={row[label]}
            key={label}
            component={getTo(row, index) ? RouterLink : 'td'}
            to={getTo(row, index)}
          >
            {row[label]}
          </StyledTableCell>
        ))}
      </StyledTableRow>
      {expansion && (
        <StyledTableRow>
          <StyledTableCell style={{ padding: 0 }} colSpan={colSpan + 1}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <ExpansionData row={row} />
            </Collapse>
          </StyledTableCell>
        </StyledTableRow>
      )}
    </>
  );
};

const Table = (props) => {
  const classes = useStyles();
  const { labels, expansion, rows } = props;
  return (
    <TableContainer>
      <MUITable className={classes.table} aria-label="table">
        <TableHead>
          <Head>
            {expansion && <StyledTableCell />}
            {labels.map((label) => (
              <StyledTableCell key={label}>
                <b>{label}</b>
              </StyledTableCell>
            ))}
          </Head>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => (
            <Rows
              {...props}
              row={row}
              index={index}
              key={index}
              colSpan={labels.length}
            />
          ))}
        </TableBody>
      </MUITable>
    </TableContainer>
  );
};

export default Table;
