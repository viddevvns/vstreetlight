/* eslint-disable react/prop-types */
import React from 'react';
import { Route, Navigate, useParams } from 'react-router-dom';
import BreadCrumb from 'components/BreadCrumb';

const ProtectedRoute = (props) => {
  const params = useParams();
  const { Component, user, path, breadCrumb, exact, ...rest } = props;
  if (user)
    return (
      <>
        <BreadCrumb breadCrumb={breadCrumb} params={params} />
        <Route
          exact
          path={path}
          element={<Component user={user} {...rest} params={params} />}
        />
      </>
    );
  return <Navigate to="/unauthorized" />;
};

export default ProtectedRoute;
