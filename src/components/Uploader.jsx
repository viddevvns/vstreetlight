/* eslint-disable no-unused-expressions */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import ReactS3Uploader from 'react-s3-uploader';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import { getPresignedUrls } from 'redux/actions/uploader';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
const IMAGE_FORMATS = [
  'image/png',
  'image/jpeg',
  'image/jpg',
  'application/pdf'
];

const ImageUpload = ({ faultId }) => {
  const [selectedFiles, setSelectedFiles] = useState();
  const [preview, setPreview] = useState();

  // create a preview as a side effect, whenever selected file is changed

  const dispatch = useDispatch();
  useEffect(() => {
    if (!selectedFiles) {
      setPreview(undefined);
      return;
    }
    const objectUrls = selectedFiles?.map((file) => URL.createObjectURL(file));

    setPreview(objectUrls);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrls);
  }, [selectedFiles]);

  const handleUpload = async () => {
    const data = await dispatch(getPresignedUrls(faultId, preview?.length));

    data.payload.resp?.[faultId].presigned_urls.forEach(async (url, index) => {
      let body = new FormData();
      body.append('file', selectedFiles[index]);
      fetch(url, {
        method: 'put',
        body
      })
        .then((response) => {
          console.log(response);
          console.log(response.url);
        })
        .catch((e) => {
          console.error(e);
        });
    });
  };

  const handleFileSelect = () => {
    const ele = document.getElementById('img-uploader');
    ele.click();
  };

  const onSelectFile = (e) => {
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFiles(undefined);
      return;
    }
    // I've kept this example simple by using the first image instead of multiple
    setSelectedFiles([...e.target.files]);
  };

  return (
    <div>
      <input
        style={{ display: 'none' }}
        id="img-uploader"
        type="file"
        multiple
        onChange={onSelectFile}
      />
      {selectedFiles &&
        preview?.map((file, index) => (
          <img height="200" key={index} alt="" src={file} />
        ))}

      {!preview ? (
        <Button variant="contained" color="primary" onClick={handleFileSelect}>
          Select files to upload
        </Button>
      ) : (
        <Button variant="contained" color="primary" onClick={handleUpload}>
          Upload
        </Button>
      )}
    </div>
  );
};

export default ImageUpload;
