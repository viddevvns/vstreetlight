/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUITypography from '@material-ui/core/Typography';
import MUIPaper from '@material-ui/core/Paper';
import { useTheme } from '@material-ui/core/styles';

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
  width: calc((100% / 2) - 20px);
  height: 350px;
  display: flex;
  flex-direction: column;

  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      width: 100%;
    }`};
`;

const Graph = styled.iframe`
  width: 100%;
  height: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.colors.border};
`;

const GraphLabel = styled(MUITypography)`
  color: ${({ theme }) => theme.colors.baseFont};
  ${({ $theme }) => `${[$theme.breakpoints.down('sm')]} {
      font-size: 12px;
    }`};
  padding: 10px 0;
`;

const GraphFrame = ({ src, title, children }) => {
  const theme = useTheme();
  return (
    <Paper $theme={theme}>
      {src && <Graph frameBorder="0" src={src} title={title} />}
      {children}
      <GraphLabel $theme={theme} variant="h6">
        {title}
      </GraphLabel>
    </Paper>
  );
};

export default GraphFrame;
