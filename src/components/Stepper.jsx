/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUIStepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import StepButton from '@material-ui/core/StepButton';

const Stepper = ({ activeStep, steps, nonLinear, handleStep, completed }) => {
  return (
    <MUIStepper nonLinear={nonLinear} activeStep={activeStep}>
      {steps.map((label, index) => {
        return (
          <Step key={label} completed={completed?.[index]}>
            {nonLinear ? (
              <StepButton onClick={handleStep(index)}>{label}</StepButton>
            ) : (
              <StepLabel>{label}</StepLabel>
            )}
          </Step>
        );
      })}
    </MUIStepper>
  );
};

export default Stepper;
