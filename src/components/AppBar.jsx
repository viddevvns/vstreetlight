/* eslint-disable react/prop-types */
import React from 'react';
import styled from 'styled-components';
import MUIAppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MUIIconButton from '@material-ui/core/IconButton';
import { Link as Vlink } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import Dark from '@material-ui/icons/Brightness4Rounded';
import Light from '@material-ui/icons/BrightnessHighRounded';
import MUIAvatar from '@material-ui/core/Avatar';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import MUIPaper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MUIMenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { useTheme } from '@material-ui/core/styles';
import { ReactComponent as Vidrona } from 'images/drawer/vidrona.svg';
import BSES from 'images/drawer/bses.jpeg';

const IconButton = styled(MUIIconButton)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const MenuButton = styled(IconButton)`
  ${({ $theme }) => `${[$theme.breakpoints.up('sm')]} {
      display: none;
    }`};
`;

const MenuItem = styled(MUIMenuItem)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const Paper = styled(MUIPaper)`
  background-color: ${({ theme }) => theme.colors.paper};
`;

const Link = styled(Vlink)`
  color: ${({ theme }) => theme.colors.baseFont};
`;

const AppBar = styled(MUIAppBar)`
  z-index: 1201;
  background-color: ${({ theme }) => theme.colors.paper};
  height: 64px;
`;

const GrowSpace = styled.div`
  flex-grow: 1;
`;

const Avatar = styled(MUIAvatar)`
  background-color: #ff5722;
`;

const Image = styled.img`
  width: 150px;
  height: 40px;
`;

function ButtonAppBar({
  drawerWidth,
  handleDrawerToggle,
  layoutTheme,
  handleLight,
  handleDark,
  user
}) {
  const onThemeClick = () => {
    if (layoutTheme === 'dark') handleLight();
    else handleDark();
  };
  const theme = useTheme();

  const [userLoggedIn, setUserLoggedIn] = React.useState(false);
  const [isMenuOpen, setMenuOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setMenuOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (
      anchorRef.current &&
      event &&
      anchorRef.current.contains(event.target)
    ) {
      return;
    }
    setMenuOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setMenuOpen(false);
    }
  }

  function eraseCookie(name) {
    document.cookie =
      name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }

  const prevOpen = React.useRef(isMenuOpen);
  React.useEffect(() => {
    if (prevOpen.current === true && isMenuOpen === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = isMenuOpen;
  }, [isMenuOpen]);

  const handleUserToggle = () => {
    setUserLoggedIn(!userLoggedIn);
  };

  const handleLogOut = () => {
    handleUserToggle();
    eraseCookie('user');
    window.location.reload();
    handleClose();
  };

  return (
    <AppBar $drawerWidth={drawerWidth} position="fixed">
      <Toolbar>
        <MenuButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          $theme={theme}
        >
          <MenuIcon />
        </MenuButton>
        <Link style={{ display: 'flex' }} to="/">
          <Vidrona width="40px" />
        </Link>
        <GrowSpace />
        {user && <Image src={BSES} />}
        <div ref={anchorRef}>
          {user && (
            <IconButton
              aria-controls={isMenuOpen ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={handleToggle}
            >
              <Avatar
                alt={user?.email_id?.charAt(0).toUpperCase()}
                src="/broken-image.jpg"
              />
            </IconButton>
          )}
        </div>
        <Popper
          open={isMenuOpen}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin:
                  placement === 'bottom' ? 'center top' : 'center bottom'
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList
                    autoFocusItem={isMenuOpen}
                    id="menu-list-grow"
                    onKeyDown={handleListKeyDown}
                  >
                    <MenuItem onClick={handleClose}>
                      <Link to="/client-info">Profile</Link>
                    </MenuItem>
                    {/* <MenuItem onClick={handleClose}>My account</MenuItem> */}
                    <MenuItem onClick={handleLogOut}>
                      <Link to="/">Logout</Link>
                    </MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
        {user && (
          <IconButton onClick={onThemeClick}>
            {(theme === 'dark' && <Light />) || <Dark />}
          </IconButton>
        )}
      </Toolbar>
    </AppBar>
  );
}

export default ButtonAppBar;
