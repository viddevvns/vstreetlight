/* eslint-disable react/prop-types */
import React from 'react';
import { Cell, ResponsiveContainer, PieChart, Pie } from 'recharts';

const PieGraph = ({ graph }) => {
  const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent,
    index
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? 'start' : 'end'}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    );
  };
  return (
    <ResponsiveContainer width="100%" height="100%">
      <PieChart>
        <Pie
          data={graph.data1}
          dataKey="value"
          cx="50%"
          cy="50%"
          innerRadius={120}
          outerRadius={140}
          fill="#82ca9d"
          label
        />
        <Pie
          data={graph.data}
          dataKey="value"
          cx="50%"
          cy="50%"
          outerRadius={110}
          fill="#8884d8"
          label
        />
      </PieChart>
    </ResponsiveContainer>
  );
};

export default PieGraph;
