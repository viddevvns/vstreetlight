const months = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  7: 'August',
  8: 'September',
  9: 'October',
  10: 'November',
  11: 'December'
};

const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

export const calculateTimeLeft = (date) => {
  let difference = new Date(date) - new Date();
  let timeLeft = 0;
  if (difference > 0) {
    timeLeft = Math.floor(difference / (1000 * 60 * 60 * 24));
    // {
    //   Days: Math.floor(difference / (1000 * 60 * 60 * 24))
    //   hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
    //   minutes: Math.floor((difference / 1000 / 60) % 60),
    //   seconds: Math.floor((difference / 1000) % 60)
    // };
  }
  return timeLeft;
};

const getDateString = (rawDate) => {
  if (!rawDate) return;
  const d = new Date(rawDate);
  const year = d.getFullYear();
  const date = d.getDate();
  const monthName = months[d.getMonth()];
  const dayName = days[d.getDay()];
  return `${dayName}, ${date} ${monthName} ${year}`;
};

export default getDateString;
