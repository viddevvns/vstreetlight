const graphsOne = {
  title: 'Maintenance Tasks Status with Tasks Severity',
  graphs: [
    {
      title: 'Task Status vs Count',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/db6b153634132fd036c4ff30a6e5b93e'
    },
    {
      title: 'Not Scheduled Tasks with Task Severity',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/17050a0d8981814f4f0f226e22deb7c7'
    },
    {
      title: 'Standby Tasks with Tasks Severity',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/c4c7d15e6183acc150f0906bbe38c7dc'
    },
    {
      title: 'Scheduled Tasks with Tasks Severity',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/2cf08ba44e31e3a270963a1fdbd04966'
    },
    {
      title: 'Ongoing Tasks with Tasks Severity',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/bb92f7c32dcf11e9bbb55b5562ed464a'
    }
  ]
};

const graphsTwo = {
  title: 'Maintenance Tasks with Budget Analysis',
  graphs: [
    {
      title: 'Maintenance Tasks with Budget Analysis',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/e58c207903ef0764992400981b20d2e2'
    },
    {
      title: 'Actual Costing vs Task Status',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/af5930eba5fc30643fb7620ec97a881b'
    },
    {
      title: 'Costing per Fault Types',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/1fde6b5e2a63e5f6e632613f7fa4a446'
    },
    {
      title: 'Estimated Costing vs Task Status',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/9109e0e176427b60e1221cb6ffe3d27a'
    }
  ]
};

const graphsThree = {
  title: 'Maintenance Tasks Assigned to Personnel',
  graphs: [
    {
      title: 'Maintenance Tasks Assigned Personnel',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/13e291a38763dfbb10f70c875d3ea2c1'
    },
    {
      title: 'Count of Task Status per Assigned Personnel',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/87d4da30355dcc4925c68ed2c975a159'
    },
    {
      title: 'Assigned Personnel vs Task Status',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/baeb64f2ee001d1e95a72da2b7ed2078'
    },
    {
      title: 'Estimated Cost and Actual Cost per Personnel',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/7145a6942d80e3faba380cb1e1fba64c'
    },
    {
      title: 'Faults Identified Weekly',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/6f649db48d5c21d5e25188af6f2a1c71'
    }
  ]
};

const assetGraphs = {
  title: 'Asset performance',
  graphs: [
    {
      title: 'Health of Asset T0015',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/bec845b0db0f0f93618090a051da00ac'
    },
    {
      title: 'Health of Asset T0024',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/3c9b809b53f912d78accefc6650703f8'
    },
    {
      title: 'Assets Exposure to Natural Calamities',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/e97e0a4e76e98941b5701b1f31e5bd54'
    },
    {
      title: "Asset's Health",
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/f1bd4b7b06401b2db6cd032296622f26'
    },
    {
      title: 'Faults per Asset',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/83d31a7392db0978e7d28ca12a2d557d'
    },
    {
      title: 'Faults Identified Weekly',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/6f649db48d5c21d5e25188af6f2a1c71'
    }
  ]
};

const lineAvailabilityGraphs = {
  title: 'Line Availability',
  graphs: [
    {
      title: 'Line Assets Tags',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/17bfabc64602b7bc16b477682ee20e85'
    },
    {
      title: 'Weekly Line Tripping Duration',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/3687546bee946cf401d203ebf3425c6c'
    },
    {
      title: 'Line Losses Incurred',
      src:
        'https://search-vns-vishlesan-4elhr6szcr6bessdov74aeeqru.eu-west-1.es.amazonaws.com/_plugin/kibana/goto/910062e10e8e87aa999d427697dcddf5'
    }
  ]
};

module.exports = {
  graphsOne,
  graphsTwo,
  graphsThree,
  assetGraphs,
  lineAvailabilityGraphs
};
